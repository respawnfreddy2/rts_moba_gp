%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: MeleeRobotTorsoMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Hip
    m_Weight: 0
  - m_Path: Hip/L.UpLeg
    m_Weight: 0
  - m_Path: Hip/L.UpLeg/L.LoLeg
    m_Weight: 1
  - m_Path: Hip/L.UpLeg/L.LoLeg/L.LoLeg 1
    m_Weight: 1
  - m_Path: Hip/L.UpLeg/L.LoLeg/L.LoLeg_end
    m_Weight: 1
  - m_Path: Hip/L.UpLeg/L.UpLeg 1
    m_Weight: 1
  - m_Path: Hip/R.UpLeg
    m_Weight: 0
  - m_Path: Hip/R.UpLeg/R.LoLeg
    m_Weight: 1
  - m_Path: Hip/R.UpLeg/R.LoLeg/R.LoLeg 1
    m_Weight: 1
  - m_Path: Hip/R.UpLeg/R.LoLeg/R.LoLeg_end
    m_Weight: 1
  - m_Path: Hip/R.UpLeg/R.UpLeg 1
    m_Weight: 1
  - m_Path: Hip/Robot
    m_Weight: 0
  - m_Path: Hip/Torso
    m_Weight: 1
  - m_Path: Hip/Torso/Hull
    m_Weight: 1
  - m_Path: Hip/Torso/Hull/Saw
    m_Weight: 1
  - m_Path: Hip/Torso/Torso_end
    m_Weight: 1
  - m_Path: L.Foot.IK
    m_Weight: 0
  - m_Path: L.Foot.IK/L.Foot
    m_Weight: 0
  - m_Path: L.Foot.IK/L.Foot.IK_end
    m_Weight: 0
  - m_Path: R.Foot.IK
    m_Weight: 0
  - m_Path: R.Foot.IK/R.Foot
    m_Weight: 0
  - m_Path: R.Foot.IK/R.Foot.IK_end
    m_Weight: 0
