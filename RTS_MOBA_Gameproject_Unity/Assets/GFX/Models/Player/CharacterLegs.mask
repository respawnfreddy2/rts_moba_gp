%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: CharacterLegs
  m_Mask: 01000000000000000000000001000000010000000000000000000000000000000000000001000000010000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Human
    m_Weight: 0
  - m_Path: Player
    m_Weight: 0
  - m_Path: Player/Hip
    m_Weight: 0
  - m_Path: Player/Hip/L.UpLeg
    m_Weight: 1
  - m_Path: Player/Hip/L.UpLeg/L.LoLeg
    m_Weight: 1
  - m_Path: Player/Hip/L.UpLeg/L.LoLeg/L.UpFoot
    m_Weight: 1
  - m_Path: Player/Hip/L.UpLeg/L.LoLeg/L.UpFoot/L.LoFoot
    m_Weight: 1
  - m_Path: Player/Hip/L.UpLeg/L.LoLeg/L.UpFoot/L.LoFoot/L.LoFoot_end
    m_Weight: 1
  - m_Path: Player/Hip/R.UpLeg
    m_Weight: 1
  - m_Path: Player/Hip/R.UpLeg/R.LoLeg
    m_Weight: 1
  - m_Path: Player/Hip/R.UpLeg/R.LoLeg/R.UpFoot
    m_Weight: 1
  - m_Path: Player/Hip/R.UpLeg/R.LoLeg/R.UpFoot/R.LoFoot
    m_Weight: 1
  - m_Path: Player/Hip/R.UpLeg/R.LoLeg/R.UpFoot/R.LoFoot/R.LoFoot_end
    m_Weight: 0
  - m_Path: Player/Hip/Waist
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/L.Shoulder
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/L.Shoulder/L.UpArm
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/L.Shoulder/L.UpArm/L.LoArm
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/L.Shoulder/L.UpArm/L.LoArm/L.Hand
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/L.Shoulder/L.UpArm/L.LoArm/L.Hand/L.UpFinger
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/L.Shoulder/L.UpArm/L.LoArm/L.Hand/L.UpFinger/L.LoFinger
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/L.Shoulder/L.UpArm/L.LoArm/L.Hand/L.UpFinger/L.LoFinger/L.LoFinger_end
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/L.Shoulder/L.UpArm/L.LoArm/L.Hand/L.UpThumb
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/L.Shoulder/L.UpArm/L.LoArm/L.Hand/L.UpThumb/L.LoThumb
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/L.Shoulder/L.UpArm/L.LoArm/L.Hand/L.UpThumb/L.LoThumb/L.LoThumb_end
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/Neck
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/Neck/Head
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/Neck/Head/Head_end
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/R.Shoulder
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/R.Shoulder/R.UpArm
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand/R.UpFinger
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand/R.UpFinger/R.LoFinger
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand/R.UpFinger/R.LoFinger/R.LoFinger_end
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand/R.UpThumb
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand/R.UpThumb/R.LoThumb
    m_Weight: 0
  - m_Path: Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand/R.UpThumb/R.LoThumb/R.LoThumb_end
    m_Weight: 0
  - m_Path: Player/L.Foot.IK
    m_Weight: 1
  - m_Path: Player/L.Foot.IK/L.Foot.IK_end
    m_Weight: 1
  - m_Path: Player/L.Hand.Controller
    m_Weight: 0
  - m_Path: Player/L.Hand.Controller/L.Hand.Controller_end
    m_Weight: 0
  - m_Path: Player/R.Foot.IK
    m_Weight: 1
  - m_Path: Player/R.Foot.IK/R.Foot.IK_end
    m_Weight: 1
  - m_Path: Player/R.Hand.Controller
    m_Weight: 0
  - m_Path: Player/R.Hand.Controller/R.Hand.Controller_end
    m_Weight: 0
