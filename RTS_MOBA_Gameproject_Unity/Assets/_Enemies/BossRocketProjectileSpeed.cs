﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts.VFX;
using UnityEngine;
using Random = System.Random;

public class BossRocketProjectileSpeed : ProjectileController
{
    protected float Animation;

    private Vector3 startPos;
    private Vector3 endPos;
    private Vector3 LastPosition;
    private bool madeRotation ;
    private int TargetRadius = 4;

    public GameObject TargetAnimation;
    private GameObject currentTargetAnimation;
    
    
    

    // Start is called before the first frame update
    void Start()
    {
        if (flashEffect != null)
        {
            var shootVfx = Instantiate(flashEffect, transform.position, Quaternion.identity);
            shootVfx.transform.forward = gameObject.transform.forward;

            var psShoot = shootVfx.GetComponent<ParticleSystem>();
            if (psShoot != null)
                Destroy(shootVfx, psShoot.main.duration);
            else
            {
                var psChild = shootVfx.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(shootVfx, psChild.main.duration);
            }
        }

        madeRotation = false;
        var player = GameObject.FindWithTag("Player");
        
        if (player != null)
        {
            var playerPos = player.transform.position;
            
            
            Random random = new Random();
            var endPosX = random.Next((int)playerPos.x -TargetRadius,(int)playerPos.x + TargetRadius);
            var endPosZ = random.Next((int)playerPos.z -TargetRadius,(int)playerPos.z + TargetRadius);
            endPos = new Vector3(endPosX, playerPos.y, endPosZ);
            
             currentTargetAnimation = Instantiate(TargetAnimation, endPos, Quaternion.identity);
             endPos.y -= 5;
        }
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (projectileSpeed != 0)
        {
            Animation += Time.deltaTime;
            Animation = Animation % 5;

            transform.position = MathParabola.Parabola(startPos, endPos, 10f, Animation / 5f);

            if (LastPosition.y > transform.position.y && !madeRotation)
                RotateRocket();

            LastPosition = transform.position;
        }
    }

    private void RotateRocket()
    {
        foreach (Transform child in transform)
        {
            var newRotation = child.rotation;
            newRotation.x += 180;
            child.rotation = newRotation;
        }

        madeRotation = true;
    }


    private void OnTriggerEnter(Collider other)
    {   
        if ( other.gameObject.CompareTag("Projectile") || other.gameObject.CompareTag("Enemy") || other.gameObject.layer == 15 || other.gameObject.CompareTag("BossTriggerBox"))
            return;

        if (other.gameObject.CompareTag("Barrel")) {
            other.transform.GetComponent<Barrel>().DoDamage(damage);
        }

        if (other.gameObject.CompareTag("Player"))
            other.gameObject.GetComponent<PlayerStats>().GetDamage(damage);

        projectileSpeed = 0;

        if (bulletHit != null)
        {
            var hitVfx = Instantiate(bulletHit, transform.position, transform.rotation);

            var psHit = hitVfx.GetComponent<ParticleSystem>();
            if (psHit != null)
                Destroy(hitVfx, psHit.main.duration);
            else
            {
                var psChild = hitVfx.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitVfx, psChild.main.duration);
            }
        }
        
        Destroy(currentTargetAnimation);
        
        Destroy(gameObject);
    }
}