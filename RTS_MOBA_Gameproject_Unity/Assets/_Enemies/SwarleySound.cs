﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SwarleySound : MonoBehaviour
{
    private AudioSource audioSource;
    private AudioClip movingSound;
    
    // Start is called before the first frame update
    void Start()
    {
        
        if (TryGetComponent<AudioSource>(out AudioSource GOaudioSource))
            audioSource = GOaudioSource;


        movingSound = SoundManager.GetSound(transform.parent.gameObject.name);

        audioSource.PlayOneShot(movingSound);

    }

    
}
