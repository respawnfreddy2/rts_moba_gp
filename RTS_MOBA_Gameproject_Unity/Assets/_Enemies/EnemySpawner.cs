﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawner : MonoBehaviour
{
    [Range(0, 600)] public float SpawnRate;
    public bool isSpawning = true;
    public float SpawnAreaSize;
    public int StopSpawning;
    public GameObject[] EnemieList;

    private bool captured;
    private float spawnTimer;

    private void Start()
    {
        spawnTimer = SpawnRate;
    }

    void Update()
    {
        if (isSpawning)
        {
            if (gameObject.transform.childCount < StopSpawning)
            {
                spawnTimer -= Time.deltaTime;
                if (spawnTimer <= 0)
                {
                    SpawnEnemy();
                    spawnTimer = SpawnRate;
                }
            }
            else
            {
                spawnTimer = SpawnRate * 2;
            }
        } else if (!isSpawning && gameObject.transform.childCount > 0 && !captured)
        {
            captured = true;
            gameObject.SetActive(false);
        }
    }

    private void SpawnEnemy()
    {
        int choose = Random.Range(0, EnemieList.Length);
        var t = Instantiate(EnemieList[choose], findSpawnPoint(), EnemieList[choose].transform.rotation) as GameObject;
        t.name = EnemieList[choose].name;
        t.transform.parent = gameObject.transform;
        t.GetComponent<EnemyStatsHandler>().WalkingWhenIdleSpherePosition = this.gameObject.transform.position;
    }

    private Vector3 findSpawnPoint()
    {
        bool foundPoint = false;
        Vector3 res = new Vector3();

        while (!foundPoint)
        {
            res = gameObject.transform.position + Random.insideUnitSphere * SpawnAreaSize;

            NavMeshHit hit;
            if (NavMesh.SamplePosition(res, out hit, 1.0f, NavMesh.AllAreas) && Vector3.Distance(res, gameObject.transform.position) <= SpawnAreaSize)
            {
                foundPoint = true;
                return hit.position;
            }
        }
        return res;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(this.transform.position, SpawnAreaSize);
    }
}

