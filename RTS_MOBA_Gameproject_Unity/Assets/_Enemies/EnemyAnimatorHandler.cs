﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimatorHandler : MonoBehaviour
{
    [HideInInspector] public Animator Animator;
    private EnemyStats EnemyStats;
    private StateController controller;

    private void Start()
    {
        Animator = GetComponentInChildren<Animator>();
        EnemyStats = GetComponent<StateController>().enemyStats;
        controller = GetComponent<StateController>();
    }

    private void FixedUpdate()
    {
        if ((gameObject.name == "MeleeRazorEnemy" || gameObject.name == "BruteEnemy") && controller.enemyStatsHandler.Health > 0)
        {
            if (gameObject.GetComponent<StateController>().currentState.name == "AttackMelee" && gameObject.GetComponent<EnemyAnimatorHandler>().Animator.GetCurrentAnimatorStateInfo(0).IsTag("Melee Attack"))
            {
                controller.navMeshAgent.isStopped = true;
            } else
            {
                controller.navMeshAgent.isStopped = false;
            }

            Transform saw = transform.Find("GFX/Hip/Torso/Hull/Saw");
            saw.transform.Rotate(32.0f, 0.0f, 0.0f, Space.Self);
        }

        if (controller.navMeshAgent.velocity.magnitude > 0.2)
        {
            Animator.SetBool("walking", true);
        } else
        {
            Animator.SetBool("walking", false);
        }
    }
}
