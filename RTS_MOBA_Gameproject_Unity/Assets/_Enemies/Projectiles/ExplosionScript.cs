﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SoundManager.PlaySound("RocketLauncherExplo");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Barrel")) {
            other.transform.GetComponent<Barrel>().DoDamage(PlayerStats.instance.Damage.GetValue());
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            if (other.TryGetComponent<EnemyStatsHandler>(out EnemyStatsHandler statsHandler))
                statsHandler.DoDamage(PlayerStats.instance.Damage.GetValue());
            Destroy(gameObject);
            return;
        }
        if (other.gameObject.CompareTag("ElectricalBox"))
        {
            other.gameObject.GetComponent<ElectricityBox>().doDamage((PlayerStats.instance.Damage.GetValue()));
        }
        if(other.gameObject.CompareTag("Player"))
            other.gameObject.GetComponent<PlayerStats>().GetDamage(50);
    }
}
