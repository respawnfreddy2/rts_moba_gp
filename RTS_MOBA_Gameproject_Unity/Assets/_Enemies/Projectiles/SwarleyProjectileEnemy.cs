﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwarleyProjectileEnemy : MonoBehaviour
{
    private Rigidbody rb;
    public float shootSpeed;
    [HideInInspector] public GameObject firePoint;
    [HideInInspector] public StateController controller;

    bool shoot = true;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Awake()
    {
        Destroy(gameObject, 3);
    }

    void Update()
    {
        if (firePoint != null && shoot)
        {
            shoot = false;
            rb.AddForce(firePoint.transform.forward * shootSpeed);
        }
        else
            rb.useGravity = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerStats>().GetDamage(controller.enemyStats.damage);
            Destroy(gameObject);
        }
        else if (!other.CompareTag("Enemy") && !other.CompareTag("Projectile"))
            Destroy(gameObject);
    }
}
