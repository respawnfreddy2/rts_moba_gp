﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunSplintSpeed : MonoBehaviour
{
    public int maxSpeed;
    public int minSpeed;

    private int speed;

    
    // Start is called before the first frame update
    void Start()
    {
        speed = Random.Range(minSpeed, maxSpeed);
        Destroy(gameObject, 5);
    }

    // Update is called once per frame
    void Update()
    {
        if(speed != 0)
            transform.position += transform.forward * (speed * Time.deltaTime);
    }
    
    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "DamageZone" || other.gameObject.name == "Player") return;

        if (other.gameObject.CompareTag("Wall")) {
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Barrel")) {
            other.transform.GetComponent<Barrel>().DoDamage(PlayerStats.instance.Damage.GetValue());
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            if (other.gameObject.TryGetComponent<EnemyStatsHandler>(out EnemyStatsHandler statsHandler))
                statsHandler.DoDamage(PlayerStats.instance.Damage.GetValue());
            Destroy(gameObject);

            return;
        }
        if (other.gameObject.CompareTag("ElectricalBox"))
        {
            other.gameObject.GetComponent<ElectricityBox>().doDamage((PlayerStats.instance.Damage.GetValue()));
            Destroy(gameObject);
        }       
    }
}
