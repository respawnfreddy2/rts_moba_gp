﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileEnemy : MonoBehaviour
{
    Rigidbody rb;
    public float shootSpeed;
    [HideInInspector] public StateController controller;

    bool shoot = true; 

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Awake()
    {
        Destroy(gameObject, 5);
    }

    void Update()
    {
        if (controller.enemyStatsHandler.FirePoint != null && shoot)
        {
            shoot = false;
            rb.AddForce(controller.enemyStatsHandler.FirePoint.transform.forward * shootSpeed);
        }
        else
            rb.useGravity = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerStats>().GetDamage(controller.enemyStats.damage);
        } else if (other.CompareTag("Projectile"))
        {
            // Do Nothing
        } else
        {
            Destroy(gameObject);
        }
    }
}
