﻿using System.Collections;
using System.Collections.Generic;
using _Scripts.VFX;
using UnityEngine;

public class ProjectileShotgun : ProjectileController
{
    // public float Speed;
    public float splinterAmount;


    // public GameObject ShootPrefab;
    // public GameObject HitPrefab;
    public GameObject splinterPrefab;
    public List<GameObject> splinters;
    public float inaccuracy;

    // Start is called before the first frame update
    void Start()
    {
        splinters = new List<GameObject>();

        for (int i = 0; i < splinterAmount; i++)
        {
            var splinter = Instantiate(splinterPrefab, transform.position, Random.rotation);

            splinter.transform.parent = gameObject.transform;
            splinter.transform.forward = gameObject.transform.forward;


            float randomX = Random.Range(-inaccuracy, inaccuracy);
            float randomY = Random.Range(-inaccuracy, inaccuracy);

            splinter.transform.Rotate(randomX, randomY, 0.0f);


            splinters.Add(splinter);
        }

        if (flashEffect != null)
        {
            var shootVfx = Instantiate(flashEffect, transform.position, Quaternion.identity);
            shootVfx.transform.forward = gameObject.transform.forward;

            var psShoot = shootVfx.GetComponent<ParticleSystem>();
            if (psShoot != null)
                Destroy(shootVfx, psShoot.main.duration);
            else
            {
                var psChild = shootVfx.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(shootVfx, psChild.main.duration);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}