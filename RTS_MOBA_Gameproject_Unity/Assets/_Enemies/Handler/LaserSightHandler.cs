﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSightHandler : MonoBehaviour
{
    private LineRenderer lr;

    private void Start()
    {
        lr = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        //lr.SetColors(Color.green, Color.green);
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            if (hit.collider)
            {
                lr.SetPosition(1, new Vector3(0, 0, hit.distance));
                if (hit.collider.CompareTag("Player"))
                {
                    //lr.SetColors(Color.red, Color.red);
                }
            } else
            {
                lr.SetPosition(1, new Vector3(0, 0, 5000));
            }
        }
    }
}
