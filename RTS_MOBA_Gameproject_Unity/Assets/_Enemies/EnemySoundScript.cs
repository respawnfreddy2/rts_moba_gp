﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySoundScript : MonoBehaviour
{

    private NavMeshAgent navMeshAgent;
    private AudioSource audioSource;
    private AudioClip movingSound;
    private float lastStepTime;
    public float stepFrequenz = 0.7f;
    
    // Start is called before the first frame update
    void Start()
    {
        lastStepTime = Time.time;
        
        if (transform.parent.gameObject.TryGetComponent<NavMeshAgent>(out NavMeshAgent GOnavMeshAgent))
            navMeshAgent = GOnavMeshAgent;

        
        if (TryGetComponent<AudioSource>(out AudioSource GOaudioSource))
            audioSource = GOaudioSource;


        movingSound = SoundManager.GetSound(gameObject.name);


    }

    // Update is called once per frame
    void Update()
    {
        if (navMeshAgent.velocity != Vector3.zero && Time.time - lastStepTime > stepFrequenz)
        {
            audioSource.PlayOneShot(movingSound);
            lastStepTime = Time.time;
        }
    }
}
