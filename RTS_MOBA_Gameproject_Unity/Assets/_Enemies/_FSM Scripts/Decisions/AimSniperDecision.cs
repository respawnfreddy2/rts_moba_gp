﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/AimSniperDecision")]
public class AimSniperDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return Decision(controller);   
    }

    private bool Decision(StateController controller)
    {
        controller.navMeshAgent.stoppingDistance = controller.enemyStats.GetAggroRange * 0.75f;
        if (controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance + 1f)
            return true;
        return false;
    }
}
