﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/MeleeStopAttackDecision")]
public class MeleeStopAttackDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return Decision(controller);
    }

    private bool Decision(StateController controller)
    {
        if (controller.GetComponent<EnemyAnimatorHandler>().Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !controller.GetComponent<EnemyAnimatorHandler>().Animator.IsInTransition(0))
        {
            controller.gameObject.GetComponent<EnemyAnimatorHandler>().Animator.SetBool("attack", false);
            return true;
        }
        else
            return false;
    }
}

