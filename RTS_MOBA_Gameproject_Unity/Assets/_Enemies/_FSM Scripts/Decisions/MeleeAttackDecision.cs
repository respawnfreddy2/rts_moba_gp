﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/MeleeAttackDecision")]
public class MeleeAttackDecision : Decision
{
    public override bool Decide(StateController controller)
    {
       return decideAttack(controller);
    }

    private bool decideAttack(StateController controller)
    {
        if (Vector3.Distance(controller.enemyStatsHandler.Target.transform.position, controller.gameObject.transform.position) <= controller.navMeshAgent.stoppingDistance)
        {
            controller.gameObject.GetComponent<EnemyAnimatorHandler>().Animator.SetBool("attack", true);
            return true;
        }
        else
            return false;
    }

}
