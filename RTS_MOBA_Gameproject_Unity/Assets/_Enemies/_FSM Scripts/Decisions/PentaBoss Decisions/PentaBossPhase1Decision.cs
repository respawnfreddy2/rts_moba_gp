﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/PentaBoss/PentaBossPhase1Decision")]
public class PentaBossPhase1Decision : Decision
{
    public override bool Decide(StateController controller)
    {
        if (controller.enemyStatsHandler.Health / controller.enemyStats.StartingHealth - 1 >= 0.6)
            return true;
        return false;
    }
}
