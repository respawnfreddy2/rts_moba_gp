﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/PentaBoss/PentaBossPhase3Decision")]
public class PentaBossPhase3Decision : Decision
{
    public override bool Decide(StateController controller)
    {
        if (controller.enemyStatsHandler.Health / controller.enemyStats.StartingHealth <= 0.3)
            return true;
        return false;
    }
}
