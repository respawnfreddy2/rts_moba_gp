﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/isDead")]
public class isDead : Decision
{
    public override bool Decide(StateController controller)
    {
        return controller.enemyStatsHandler.isDead;
    }
}
