﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/ChaseAggressiveDecision")]
public class ChaseAggressiveDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return EnemyInRange(controller);
    }

    public bool EnemyInRange(StateController controller)
    {
        if (Vector3.Distance(controller.gameObject.transform.position, controller.enemyStatsHandler.Target.transform.position) <= controller.enemyStats.GetAggroRange)
        {
            return true;
        }
        return false;
    }
}
