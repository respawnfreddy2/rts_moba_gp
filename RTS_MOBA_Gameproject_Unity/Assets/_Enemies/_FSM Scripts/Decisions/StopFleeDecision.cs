﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/StopFleeDecision")]
public class StopFleeDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool flee = false;

        if (controller.enemyStatsHandler.Health >= controller.enemyStats.FleeHealthValue / 2 && Vector3.Distance(controller.gameObject.transform.position, controller.enemyStatsHandler.Target.transform.position) > controller.enemyStats.LooseAggroRange)
        {
            flee = true;
        }

        return flee;
    }
}
