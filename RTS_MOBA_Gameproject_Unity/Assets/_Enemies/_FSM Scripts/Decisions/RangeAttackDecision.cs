﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/RangeAttackDecision")]
public class RangeAttackDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return decide_(controller);
    }

    private bool decide_(StateController controller)
    {
        if (Vector3.Distance(controller.enemyStatsHandler.Target.transform.position, controller.gameObject.transform.position) >= controller.navMeshAgent.stoppingDistance &&
            Vector3.Distance(controller.enemyStatsHandler.Target.transform.position, controller.gameObject.transform.position) <= controller.enemyStats.LooseAggroRange)
        {
            return true;
        }

        return false;
    }
}
