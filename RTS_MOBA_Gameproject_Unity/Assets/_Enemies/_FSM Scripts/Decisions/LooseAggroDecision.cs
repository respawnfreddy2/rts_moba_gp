﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/LooseAggroDecision")]
public class LooseAggroDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return LooseAggro(controller);
    }

    private bool LooseAggro(StateController controller)
    {
        if (Vector3.Distance(controller.gameObject.transform.position, controller.enemyStatsHandler.Target.transform.position) >= controller.enemyStats.LooseAggroRange)
        {
            return true;
        }
        return false;
    }
}
