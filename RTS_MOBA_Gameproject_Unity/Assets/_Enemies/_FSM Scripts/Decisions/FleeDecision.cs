﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/FleeDecision")]
public class FleeDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool flee = false;

        if (controller.enemyStatsHandler.Health <= controller.enemyStats.FleeHealthValue)
        {
            flee = true;
        }

        return flee;
    }
}
