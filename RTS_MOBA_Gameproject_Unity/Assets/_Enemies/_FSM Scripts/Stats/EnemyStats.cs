﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Stats/DefaultEnemyStats")]
public class EnemyStats : ScriptableObject
{
    //Hide not in Inspector
    public float StartingHealth;
    public float StartingEnergy;
    public float ExpValue;
    public float GetAggroRange;
    public float LooseAggroRange;
    public float FleeHealthValue;
    public float attackRate;
    public float damage;
    
    // public float reward;


    // Attack Decision
    public bool isAgressiveBehavior;

    // Idle Beh. 
    public bool isWalkingWhenIdle;
    [Range(0, 6000)] public float idleWaitSecTimeMin;
    [Range(0, 6000)] public float idleWaitSecTimeMax;
    public float WalkingWhenIdleSphereSize;

    public float MinDistanceSwarm;
    public float MaxDistanceSwarm;
}
