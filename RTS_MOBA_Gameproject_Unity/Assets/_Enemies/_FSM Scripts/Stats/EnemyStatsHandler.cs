﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class EnemyStatsHandler : MonoBehaviour
{
    [HideInInspector] public float Health;
    [HideInInspector] public float Energy;
    [HideInInspector] public GameObject Target;
    [HideInInspector] public Transform FirePoint;
    [HideInInspector] public Vector3 WalkingWhenIdleSpherePosition;
    [HideInInspector] public bool isDead = false;

    private StateController controller;
    private EnemyStats enemyStats;
    private float attackRateCounter;

    public GameObject DeathAnimation;
    public GameObject [] lootList;

    public Material AppearMaterial;
    private Material normalMaterial;
    private float currentAppearAmount;
    private float minAppearAmount;

    public GameObject getDamageEffect;

    
    private void Start()
    {
        controller = GetComponent<StateController>();
        enemyStats = controller.enemyStats;
        Health = enemyStats.StartingHealth;
        Energy = enemyStats.StartingEnergy;
        Target = GameObject.FindGameObjectWithTag("Player");
        if (transform.name == "RangeBasicEnemy")
        {
            FirePoint = transform.Find("enemy_robot_ranged/Hip/Torso/Hull/Gun/FirePoint");

        }

        StartCoroutine(addHealth());
        
        InitMaterial();
        StartCoroutine(IncreaseMaterial());
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && gameObject.name == "BruteEnemy")
        {
            attackRateCounter -= Time.deltaTime;
            if (attackRateCounter <= 0)
            {
                other.GetComponent<Rigidbody>().AddForce((other.transform.position - gameObject.transform.position) * 100, ForceMode.Impulse);
                attackRateCounter = enemyStats.attackRate;
                Target = other.gameObject;
                other.GetComponent<PlayerStats>().GetDamage(enemyStats.damage);
            }
        }
        else {

        }
    }

    IEnumerator addHealth()
    {
        while (true)
        {
            // loops forever...
            if (Health < enemyStats.StartingHealth)
            {
                // if health < 100...
                Health += 1f; // increase health and wait the specified time
                yield return new WaitForSeconds(1);
            }
            else
            {
                // if health >= 100, just yield 
                yield return null;
            }
        }
    }

    public void DoDamage(float amount)
    {
        SoundManager.PlaySound("HitSound");
        Health -= amount;
        
        if (getDamageEffect != null)
        {
            GameObject damageAnimation = Instantiate(getDamageEffect, transform.position, Quaternion.identity);
            Destroy(damageAnimation, 1f);

        }

        if (Health <= 0)
        {
            Target.GetComponent<PlayerStats>().GainEP(enemyStats.ExpValue);
            DestroyThis();
        }
    }


    private void DestroyThis()
    {
        SoundManager.PlaySound("Shutdown");
        
        isDead = true;
        if (GetComponent<CapsuleCollider>() != null)
            GetComponent<CapsuleCollider>().enabled = false;
        if (controller.navMeshAgent != null)
            controller.navMeshAgent.isStopped = true;

        //give every part a rigidBody and a collider
        Transform[] allChildren = GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            if (child.gameObject.GetComponent<MeshRenderer>() != null )
            {
                var meshCollider = child.gameObject.AddComponent<MeshCollider>();
                meshCollider.convex = true;
            }
            if (child.gameObject.GetComponent<Rigidbody>() == null)
            {
                var rigidBody = child.gameObject.AddComponent<Rigidbody>();
                rigidBody.mass = 3;
            } else
            {
                child.gameObject.GetComponent<Rigidbody>().mass = 3;
            }

        }

        //spawn animation
        if (DeathAnimation != null)
        {
            GameObject vfxContainer = GameObject.Find("vfx_Container");
            //TODO:: calculate the center of the object better
            var pos = transform.position;
            pos.y += 1;
            GameObject deathAnim = Instantiate(DeathAnimation,  pos, Quaternion.identity, vfxContainer.transform);

            Destroy(deathAnim, 2);
        }

        // spawn gear pickup
        if(lootList != null && lootList.Length != 0) {
            int index = Random.Range(0, lootList.Length);
            GameObject loot = lootList [index];
            Instantiate(loot, transform.position, transform.rotation);
        }


        this.transform.SetParent(null);


        Destroy(this.gameObject, 2f);
    }
    
    private void InitMaterial()
    {
        
        currentAppearAmount = 1;
        minAppearAmount = -1;
        AppearMaterial.SetFloat("Vector1_147168D", currentAppearAmount);
        
        //give every part the appearMaterial
        Transform[] allChildren = GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            if (child.gameObject.GetComponent<MeshRenderer>() != null)
            {
                normalMaterial = child.gameObject.GetComponent<MeshRenderer>().material;
                child.gameObject.GetComponent<MeshRenderer>().material = AppearMaterial;
            }
        }
    }
    
    IEnumerator IncreaseMaterial()
    {
        while (true)
        {
            if (currentAppearAmount > minAppearAmount)
            {
                currentAppearAmount -= 0.03f;
                IncreaseAppearMaterial();
                yield return new WaitForSeconds(0.05f);
            }
            else
            {
                ResetMaterialsToDefault();
                yield return null;
            }
        }
    }

    private void ResetMaterialsToDefault()
    {
        Transform[] allChildren = GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            if (child.gameObject.GetComponent<MeshRenderer>() != null)
            {
                child.gameObject.GetComponent<MeshRenderer>().material = normalMaterial;
            }
        }
    }
    private void IncreaseAppearMaterial()
    {
        Transform[] allChildren = GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            if (child.gameObject.GetComponent<MeshRenderer>() != null)
            {
                child.gameObject.GetComponent<MeshRenderer>().material.SetFloat("Vector1_147168D", currentAppearAmount);
               
            }
        }
    }
}