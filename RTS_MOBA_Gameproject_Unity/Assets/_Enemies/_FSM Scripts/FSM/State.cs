﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/State")]
public class State : ScriptableObject
{
    public Action[] actions;
    public Transition[] transitions;
    public Color sceneGizmoColor = Color.gray;

    public void UpdateState(StateController controller)
    {
        if (controller != null)
        {
            DoActions(controller);
            CheckTransitions(controller);
        }
    }

    private void DoActions(StateController controller)
    {
        foreach (var action in actions)
        {
            action.Act(controller);
        }
    }

    private void CheckTransitions(StateController controller)
    {
        foreach (var trans in transitions)
        {
                bool decisionSucceeded = trans.decision.Decide(controller);

                if (decisionSucceeded)
                {
                    controller.TransitionToState(trans.trueState);
                }
                else
                {
                    controller.TransitionToState(trans.falseState);
                }
        }
    }
}
