﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Profiling;

public class StateController : MonoBehaviour
{
    public State currentState;
    public State remainState;
    public EnemyStats enemyStats;
    public GameObject head;
    public Light StateLight;

    [HideInInspector] public EnemyStatsHandler enemyStatsHandler;
    [HideInInspector] public NavMeshAgent navMeshAgent;
    [HideInInspector] public float stateTimeElapsed;

    public float waitTime = 0.01f;
    private float tikker;

    public bool aiActive = true;

    // Start is called before the first frame update
    void Awake()
    {


        enemyStatsHandler = GetComponent<EnemyStatsHandler>();
        if (GetComponent<NavMeshAgent>() != null)
            navMeshAgent = GetComponent<NavMeshAgent>();
        else
            navMeshAgent = gameObject.GetComponentInChildren<NavMeshAgent>();

        aiActive = false;
        StartCoroutine(waitBySpawn());
        aiActive = true;
    }

    private IEnumerator waitBySpawn()
    {
        yield return new WaitForSeconds(2);
    }

    private void Update()
    {
        if (gameObject != null && currentState != null)
            {
                if (!aiActive)
                    return;
                currentState.UpdateState(this);

            }
    }

    public void TransitionToState(State nextState)
    {
        if (StateLight != null)
            StateLight.color = currentState.sceneGizmoColor;

        if (nextState != remainState)
        {
            currentState = nextState;
            OnExitState();
        }
    }

    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        if (stateTimeElapsed >= duration)
        {
            stateTimeElapsed = 0;
            return true;
        }
        else
            return false;
    }

    private void OnExitState()
    {
        // stateTimeElapsed = 0;
    }

    void OnDrawGizmos()
    {
        if (currentState != null)
        {
            Gizmos.color = currentState.sceneGizmoColor;
            Gizmos.DrawWireSphere(this.transform.position, enemyStats.GetAggroRange);
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(gameObject.GetComponent<EnemyStatsHandler>().WalkingWhenIdleSpherePosition, enemyStats.WalkingWhenIdleSphereSize);
        }
    }

}