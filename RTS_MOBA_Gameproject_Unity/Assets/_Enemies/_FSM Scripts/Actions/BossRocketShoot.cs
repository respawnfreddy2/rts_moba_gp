﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/BossRocketShoot")]
public class BossRocketShoot : Action
{
    private bool switchReload;

    private PentaBossScript BossScript;
    private GameObject vfxContainer;
    private float timer;
    private int currentRocket;

    public override void Act(StateController controller)
    {
        timer += Time.deltaTime;

        BossScript = controller.gameObject.GetComponent<PentaBossScript>();

        if (timer >= BossScript.rocketAttackRate)
        {
            if (currentRocket < BossScript.bossRocketCount)
            {
                timer = 0;
                shoot(controller);
            }
        }
        if (currentRocket == BossScript.bossRocketCount)
        {
            if (timer > BossScript.reloadTime)
            {
                currentRocket = 0;
                timer = 0;
            }
        }
    }

    private void shoot(StateController controller)
    {
        currentRocket++;

        vfxContainer = GameObject.Find("vfx_Container");

        var proj = Instantiate(BossScript.rocket, BossScript.turretFirePoint.transform.position + (Vector3.up), Quaternion.identity) as GameObject;

        proj.gameObject.transform.parent = vfxContainer.transform;
    }
}
