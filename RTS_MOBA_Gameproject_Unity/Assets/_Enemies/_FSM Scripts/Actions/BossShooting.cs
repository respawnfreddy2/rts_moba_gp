﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.VFX;

[CreateAssetMenu(menuName = "PluggableAI/Actions/BossShooting")]
public class BossShooting : Action
{
    private bool switchReload;

    private PentaBossScript BossScript;
    private GameObject vfxContainer;
    private float timer;

    public override void Act(StateController controller)
    {
        timer += Time.deltaTime;
        
        BossScript = controller.gameObject.GetComponent<PentaBossScript>();
        
        if (timer >= controller.enemyStats.attackRate)
        {
            if (BossScript.currentBulletCount < BossScript.bulletAmount)
            {
                timer = 0;
                shoot(controller);
            }
        }
        if (BossScript.currentBulletCount == BossScript.bulletAmount)
        {
            if (timer > BossScript.reloadTime)
            {
                BossScript.currentBulletCount = 0;
                timer = 0;
            }
        }
    }

    private void shoot(StateController controller)
    {
        BossScript.currentBulletCount++;

        SoundManager.PlaySound("LaserShot");
        vfxContainer = GameObject.Find("vfx_Container");

        float randomX = Random.Range(-BossScript.inaccuracy, BossScript.inaccuracy);
        float randomY = Random.Range(-BossScript.inaccuracy, BossScript.inaccuracy);
        var proj = Instantiate(BossScript.shootProjectile, BossScript.turretFirePoint.transform) as GameObject;
        proj.GetComponent<AutomaticProjectileController>().damage = controller.enemyStats.damage;
        proj.transform.Rotate(randomX, randomY, 0);
        proj.gameObject.transform.parent = vfxContainer.transform;
    }
}
