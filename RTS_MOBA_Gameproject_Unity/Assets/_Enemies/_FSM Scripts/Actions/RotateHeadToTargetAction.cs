﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/RotateHeadToTargetAction")]
public class RotateHeadToTargetAction : Action
{

    public override void Act(StateController controller)
    {
        rotate(controller);
    }

    private void rotate(StateController controller)
    {
        controller.transform.LookAt(controller.enemyStatsHandler.Target.transform);
    }
}
