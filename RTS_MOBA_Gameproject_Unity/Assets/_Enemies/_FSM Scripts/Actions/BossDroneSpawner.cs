﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

[CreateAssetMenu(menuName = "PluggableAI/Actions/PentaBoss/BossDroneSpawner")]
public class BossDroneSpawner : Action
{
    public int droneAmount;
    public int droneMinAmount = 2; 
    public float waitTime;

    private TimerHandler timer;
    private const int MAX_ITERATIONS = 2000;
    private List<Vector3> spawnPositions = new List<Vector3>();

    private float tikker;

    [SerializeField] private GameObject[] EnemyTypes;

    public override void Act(StateController controller)
    {
        tikker -= Time.deltaTime;

        if (tikker <= 0 && controller.gameObject.GetComponent<PentaBossScript>().droneContainter.gameObject.transform.childCount <= droneMinAmount)
        {
            spwanDronesFromTurret(controller);
            tikker = waitTime;
        }
    }

    private void spwanDronesFromTurret(StateController controller)
    {
        Debug.Log("Spawning Drones");
        int count = 0;

        while (count < droneAmount)
        {
            count++;
            GameObject point = controller.gameObject.GetComponent<PentaBossScript>().turretDrownPoint;

            int choosenEnemy = UnityEngine.Random.Range(0, EnemyTypes.Length);
            
            var e = Instantiate(EnemyTypes[choosenEnemy], point.gameObject.transform) as GameObject;
            StateController enemySC = e.GetComponent<StateController>();
            e.transform.parent = controller.gameObject.GetComponent<PentaBossScript>().droneContainter.transform;
            enemySC.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, controller.gameObject.GetComponent<PentaBossScript>().spawnForce));
            enemySC.enemyStatsHandler.WalkingWhenIdleSpherePosition = controller.transform.position;
        }
    }

    private void spawnEnemies(StateController controller)
    {
        spawnPositions.Clear();

        while (spawnPositions.Count < droneAmount)
        {
            spawnPositions.Add(isRandomWalkablePointInSphere(controller));
        }

        if (spawnPositions.Count == droneAmount)
        {
            instatiateEnemies(controller);
        }

    }

    private void instatiateEnemies(StateController controller)
    {
        foreach (var pos in spawnPositions)
        {
            int choosenEnemy = UnityEngine.Random.Range(0, EnemyTypes.Length);

            var e = Instantiate(EnemyTypes[choosenEnemy], controller.transform) as GameObject;
            StateController enemySC = e.GetComponent<StateController>();
            enemySC.enemyStatsHandler.WalkingWhenIdleSpherePosition = controller.transform.position;
            e.transform.position = pos;
        }
    }

    private Vector3 isRandomWalkablePointInSphere(StateController controller)
    {
        GameObject Target = controller.gameObject;
        bool foundPoint = false;
        int iteration = 0;
        Vector3 res = new Vector3();

        while (!foundPoint && iteration < MAX_ITERATIONS)
        {
            iteration++;

            res = Target.transform.position + UnityEngine.Random.insideUnitSphere * controller.enemyStats.MaxDistanceSwarm;

            NavMeshHit hit;
            if (NavMesh.SamplePosition(res, out hit, controller.enemyStats.MaxDistanceSwarm, 1))
            {
                float disTarget = Vector3.Distance(Target.transform.position, hit.position);

                if (disTarget >= controller.enemyStats.MinDistanceSwarm &&
                    disTarget <= controller.enemyStats.MaxDistanceSwarm)
                {
                    foundPoint = true;
                    return hit.position;
                }
            }
        }
        return controller.gameObject.transform.position;
    }
}
