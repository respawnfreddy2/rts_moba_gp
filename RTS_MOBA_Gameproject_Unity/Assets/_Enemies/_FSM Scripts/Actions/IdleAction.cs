﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu (menuName = "PluggableAI/Actions/IdleAction")]
public class IdleAction : Action
{
    bool wait = true;

    public override void Act(StateController controller)
    {
        if (controller.enemyStats.isWalkingWhenIdle)
            controller.StartCoroutine(isWalkingWhenIdle(controller));
    }

    private IEnumerator isWalkingWhenIdle(StateController controller)
    {
        if (controller.navMeshAgent.remainingDistance < 2 && wait)
        {
            if (wait)
            {
                yield return new WaitForSecondsRealtime(Random.Range(controller.enemyStats.idleWaitSecTimeMin, controller.enemyStats.idleWaitSecTimeMax));
                wait = false;
            }
                
                
        }
        else if (controller.navMeshAgent.remainingDistance < 2 && !wait)
        {
            controller.navMeshAgent.SetDestination(isRandomWalkablePointInSphere(controller));
            wait = true;
        }
    }

    private Vector3 isRandomWalkablePointInSphere(StateController controller)
    {
        bool foundPoint = false;
        Vector3 res = new Vector3();

        while (!foundPoint)
        {
            res = controller.GetComponent<EnemyStatsHandler>().WalkingWhenIdleSpherePosition + Random.insideUnitSphere * controller.enemyStats.WalkingWhenIdleSphereSize;

            NavMeshHit hit;
            if (NavMesh.SamplePosition(res, out hit, 1.0f, NavMesh.AllAreas) && Vector3.Distance(res, controller.transform.position) > controller.enemyStats.WalkingWhenIdleSphereSize / 4)
            {
                foundPoint = true;
                return hit.position;
            }
        }
        return res;
    }
}
