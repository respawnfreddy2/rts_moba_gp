﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/MeleeRazorAttackAction")]
public class MeleeRazorAttackAction : Action
{
    private bool check = false;

    public override void Act(StateController controller)
    {
        // Attack(controller);
    }

    private void Attack(StateController controller)
    {
        if (!check)
        {
            controller.gameObject.GetComponent<EnemyAnimatorHandler>().Animator.SetBool("attack", true);
            check = true;
        }

        if (controller.CheckIfCountDownElapsed(controller.enemyStats.attackRate) && check)
            controller.gameObject.GetComponent<EnemyAnimatorHandler>().Animator.SetBool("attack", true);
    }
}
