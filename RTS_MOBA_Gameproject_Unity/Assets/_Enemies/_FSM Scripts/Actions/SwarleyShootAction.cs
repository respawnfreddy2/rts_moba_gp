﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.VFX;

[CreateAssetMenu(menuName = "PluggableAI/Actions/SwarleyShootAction")]
public class SwarleyShootAction : Action
{
    public GameObject projectile;
    private GameObject vfxContainer;
    public float inaccuracy;

    public override void Act(StateController controller)
    {
        if (controller.CheckIfCountDownElapsed(controller.enemyStats.attackRate))
            shoot(controller);
    }

    private void shoot(StateController controller)
    {
        SoundManager.PlaySound("LaserShot");
        vfxContainer = GameObject.Find("vfx_Container");
        // float randomX = UnityEngine.Random.Range(-inaccuracy, inaccuracy);
        // float randomY = UnityEngine.Random.Range(-inaccuracy, inaccuracy);

        var bulletInstance = Instantiate(projectile, controller.gameObject.transform.position + new Vector3(0,1.5f,1f), controller.gameObject.transform.rotation, vfxContainer.transform) as GameObject;
        bulletInstance.GetComponent<AutomaticProjectileController>().damage = controller.enemyStats.damage;
        // bulletInstance.transform.Rotate(randomX, randomY, 0.0f);
        // bulletInstance.GetComponent<SwarleyProjectileEnemy>().controller = controller;
        // bulletInstance.GetComponent<SwarleyProjectileEnemy>().firePoint = controller.gameObject;
    }
}
