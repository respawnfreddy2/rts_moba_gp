﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "PluggableAI/Actions/FleeAction")]
public class FleeAction : Action
{
    public override void Act(StateController controller)
    {
        if (controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance + 2)
            controller.navMeshAgent.SetDestination(flee(controller));
    }

    private Vector3 flee(StateController controller)
    {
        bool foundPoint = false;
        Vector3 res = new Vector3();

        while (!foundPoint)
        {
            res = controller.gameObject.transform.position + Random.insideUnitSphere * (controller.enemyStats.LooseAggroRange * 2);

            NavMeshHit hit;
            if (NavMesh.SamplePosition(res, out hit, 1.0f, NavMesh.AllAreas) && Vector3.Distance(res, controller.enemyStatsHandler.Target.transform.position) > controller.enemyStats.LooseAggroRange)
            {
                foundPoint = true;
                return hit.position;
            }
        }
        return res;
    }
}
