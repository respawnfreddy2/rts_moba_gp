﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/HoldDistance")]
public class HoldDistanceAction : Action
{
    public override void Act(StateController controller)
    {
        // holdDistance(controller);
    }

    private void holdDistance(StateController controller)
    {
        if (Vector3.Distance(controller.enemyStatsHandler.Target.transform.position, controller.gameObject.transform.position) <= controller.navMeshAgent.stoppingDistance)
        {
            Vector3 newPos = controller.transform.position + (controller.transform.position - controller.enemyStatsHandler.Target.transform.position);
            controller.navMeshAgent.SetDestination(newPos);
        }
    }
}
