﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.VFX;

[CreateAssetMenu(menuName = "PluggableAI/Actions/ShootBasicAction")]
public class ShootBasicAction : Action
{
    public GameObject projectile;
    private GameObject vfxContainer;

    public float inaccuracy;

    public override void Act(StateController controller)
    {
        if (controller.CheckIfCountDownElapsed(controller.enemyStats.attackRate))
        {
            shoot(controller);
        }

    }

    private void shoot(StateController controller)
    {
        float randomX = Random.Range(-inaccuracy, inaccuracy);
        float randomY = Random.Range(-inaccuracy, inaccuracy);
        vfxContainer = GameObject.Find("vfx_Container");

        if (controller.GetComponent<EnemyAnimatorHandler>() != null)
            controller.GetComponent<EnemyAnimatorHandler>().Animator.SetTrigger("attack");

        GameObject bulletInstance = Instantiate(projectile, controller.enemyStatsHandler.FirePoint.transform.position + new Vector3(0,0,1), controller.enemyStatsHandler.FirePoint.transform.rotation, vfxContainer.transform) as GameObject;
        bulletInstance.GetComponent<AutomaticProjectileController>().ignoreEnemy = false;
        bulletInstance.transform.Rotate(randomX, randomY, 0.0f);

        
        bulletInstance.GetComponent<AutomaticProjectileController>().damage = controller.enemyStats.damage;
        SoundManager.PlaySound("LaserShot");
        // bulletInstance.GetComponent<SwarleyProjectileEnemy>().controller = controller;
        // bulletInstance.GetComponent<SwarleyProjectileEnemy>().firePoint = controller.enemyStatsHandler.FirePoint.gameObject;
    }
}
