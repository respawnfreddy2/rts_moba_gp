﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[CreateAssetMenu(menuName = "PluggableAI/Actions/CircleAroundThePlayerAction")]
public class CircleAroundThePlayerAction : Action
{
    private bool firstWalk = true;
    private const int MAX_ITERATIONS = 1000;

    public override void Act(StateController controller)
    {
        CircleAround(controller);
    }

    private void CircleAround(StateController controller)
    {
        if (firstWalk)
        {
            controller.navMeshAgent.SetDestination(isRandomWalkablePointInSphere(controller));
            firstWalk = false;
        }


        if (controller.navMeshAgent.remainingDistance <= 2f && !firstWalk)
            controller.navMeshAgent.SetDestination(isRandomWalkablePointInSphere(controller));
    }

    private Vector3 isRandomWalkablePointInSphere(StateController controller)
    {
        GameObject Target = controller.GetComponent<EnemyStatsHandler>().Target;
        bool foundPoint = false;
        int iteration = 0;
        Vector3 res = new Vector3();

        while (!foundPoint && iteration < MAX_ITERATIONS)
        {
            iteration++;

            res = Target.transform.position + UnityEngine.Random.insideUnitSphere * controller.enemyStats.MaxDistanceSwarm;

            if (Vector3.Distance(res, Target.transform.position) >= controller.enemyStats.MinDistanceSwarm)
            {
                NavMeshHit hit;
                if (NavMesh.SamplePosition(res, out hit, controller.enemyStats.MaxDistanceSwarm, 1))
                {
                    float disTarget = Vector3.Distance(Target.transform.position, hit.position);

                    if (disTarget >= controller.enemyStats.MinDistanceSwarm &&
                        disTarget <= controller.enemyStats.MaxDistanceSwarm &&
                        Vector3.Distance(controller.gameObject.transform.position , hit.position) <= controller.enemyStats.MinDistanceSwarm)
                    {
                        foundPoint = true;
                        return hit.position;
                    }
                }
            }
        }
        return controller.gameObject.transform.position;
    }
}
