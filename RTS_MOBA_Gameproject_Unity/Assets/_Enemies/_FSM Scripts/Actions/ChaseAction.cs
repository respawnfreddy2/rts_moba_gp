﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "PluggableAI/Actions/ChaseAction")]
public class ChaseAction : Action
{
    public override void Act(StateController controller)
    {
        if (controller.CheckIfCountDownElapsed(0.5f))
            chase(controller);
    }

    private void chase(StateController controller)
    {
        controller.navMeshAgent.isStopped = false;
        controller.navMeshAgent.SetDestination(controller.enemyStatsHandler.Target.transform.position);
    }
}
