﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PentaBossScript : MonoBehaviour
{
    public StateController controller;
    public GameObject turret;
    public GameObject turretFirePoint;
    public GameObject turretDrownPoint;
    public GameObject droneContainter;
    public GameObject shootProjectile;
    public GameObject rocket;
    public float inaccuracy;
    public float spawnForce;
    public float reloadTime;
    public int bulletAmount;
    public float rocketAttackRate;
    public float rocketReloadRate;
    public int bossRocketCount;

    [HideInInspector] public int currentBulletCount = 0;

    private void Update()
    {
        if (controller.enemyStatsHandler.Health <= 0)
            WinningScreen.gameWon = true;
        controller.aiActive = true;
    }
}
