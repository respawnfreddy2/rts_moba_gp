﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NewStatPanelScript : MonoBehaviour
{
    public Image Healthbar;
    public Image Waterbar;
    public Image EPbar;
    private PlayerStats playerStats;
    private PlayerController playerController;

    private float latestHealthpoints;
    private float latestWaterpoints;
    private float latestEPpoints;
    private float LatestMoney;
    private float currentLevel;


    public TextMeshProUGUI WaterBarText;
    public TextMeshProUGUI HealthBarText;
    public TextMeshProUGUI MoneyText;
    public TextMeshProUGUI AmmunitionText;
    public TextMeshProUGUI EpBarText;


    void Start()
    {
        playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        
        latestHealthpoints = playerStats.currentHealth;
        latestWaterpoints = playerStats.currentWater;
        latestEPpoints = playerStats.experience;
        EPbar.fillAmount = 0;
        MoneyText.text = "" + playerStats.Money;
        currentLevel = playerStats.level;
    }

    // Update is called once per frame
    void Update()
    {
        if (latestHealthpoints != playerStats.currentHealth)
        {
            latestHealthpoints = (int) playerStats.currentHealth;
            Healthbar.fillAmount = latestHealthpoints / playerStats.MaxHealth.GetValue();
            HealthBarText.text = "" +  latestHealthpoints + "/" + playerStats.MaxHealth.GetValue();
        }

        if (latestWaterpoints != playerStats.currentWater)
        {
            latestWaterpoints = (int) playerStats.currentWater;
            Waterbar.fillAmount = latestWaterpoints / playerStats.MaxWater.GetValue();
            WaterBarText.text = "" + latestWaterpoints + "/" + playerStats.MaxWater.GetValue();
        }

        if (latestEPpoints != playerStats.experience)
        {
            latestEPpoints = playerStats.experience;
            if (latestEPpoints != 0)
                EPbar.fillAmount = (latestEPpoints / playerStats.levelUpEP);
            else
                EPbar.fillAmount = 0;
        }

        if (LatestMoney != playerStats.Money)
        {
            LatestMoney = playerStats.Money;
            MoneyText.text = "" + LatestMoney;
        }

        if (currentLevel != playerStats.level)
        {
            currentLevel = playerStats.level;
            EpBarText.text ="lvl" +  currentLevel;
        }

        AmmunitionText.text = "" + playerController.clipAmmunition + "/" + playerStats.ammunitionSupply;

    }
}