﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip Shotgun, AutomaticGun, Reload, Knife,RocketLauncher, GetWater, TowerDeactivate, TowerFalling, Steps, LaserShot, HitSound, RocketLauncherExplo, GetDamage,OpenChest, Shutdown, Roll;
    public static AudioClip RobotStep, SwarleySound;
    private static AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {

        Shotgun = Resources.Load<AudioClip>("Sounds/ShotgunSound");
        AutomaticGun = Resources.Load<AudioClip>("Sounds/GunshotSound");
        Reload = Resources.Load<AudioClip>("Sounds/ReloadSound");
        Knife = Resources.Load<AudioClip>("Sounds/Knife");
        RocketLauncher = Resources.Load<AudioClip>("Sounds/RocketLauncher");
        GetWater = Resources.Load<AudioClip>("Sounds/GetWater");
        TowerDeactivate = Resources.Load<AudioClip>("Sounds/TowerDeactivate");
        TowerFalling = Resources.Load<AudioClip>("Sounds/TowerFalling");
        Steps = Resources.Load<AudioClip>("Sounds/Steps");
        LaserShot = Resources.Load<AudioClip>("Sounds/LaserShot");
        HitSound = Resources.Load<AudioClip>("Sounds/HitSound");
        RocketLauncherExplo = Resources.Load<AudioClip>("Sounds/RocketLauncherExplo");
        GetDamage = Resources.Load<AudioClip>("Sounds/GetDamage");
        OpenChest = Resources.Load<AudioClip>("Sounds/OpenChest");
        RobotStep = Resources.Load<AudioClip>("Sounds/RobotStep");
        Shutdown = Resources.Load<AudioClip>("Sounds/Shutdown");
        SwarleySound = Resources.Load<AudioClip>("Sounds/SwarleySound");
        Roll = Resources.Load<AudioClip>("Sounds/Roll");


    
        audioSource = GetComponent<AudioSource>();
    }
    

    public static AudioClip GetSound(string name)
    {
        if(!name.Contains("Swarley"))
                return RobotStep;
        return SwarleySound;
    }

    public static void PlaySound(String clip)
    {
        switch (clip)
        {
            case"vfx_AutomaticGun":
                audioSource.PlayOneShot(AutomaticGun);
                break;
            case"ProjectileShotgun":
                audioSource.PlayOneShot(Shotgun);
                break;
            case"Reload":
                audioSource.PlayOneShot(Reload);
                break;
            case"Knife":
                audioSource.PlayOneShot(Knife);
                break;
            case"RocketLauncher":
                audioSource.PlayOneShot(RocketLauncher);
                break;
            case"GetWater":
                audioSource.PlayOneShot(GetWater);
                break;
            case"TowerDeactivate":
                audioSource.PlayOneShot(TowerDeactivate);
                break;
            case"TowerFalling":
                audioSource.PlayOneShot(TowerFalling);
                break;
            case"Steps":
                audioSource.PlayOneShot(Steps);
                break;
            case"LaserShot":
                audioSource.PlayOneShot(LaserShot);
                break;
            case"HitSound":
                audioSource.PlayOneShot(HitSound);
                break;
            case "RocketLauncherExplo":
                audioSource.PlayOneShot(RocketLauncherExplo);
                break;
            case "GetDamage":
                audioSource.PlayOneShot(GetDamage);
                break;
            case "OpenChest":
                audioSource.PlayOneShot(OpenChest);
                break;
            case "Shutdown":
                audioSource.PlayOneShot(Shutdown);
                break;
            case "Roll":
                audioSource.PlayOneShot(Roll);
                break;
            default:
                break;
        }
        
    }
    
}
