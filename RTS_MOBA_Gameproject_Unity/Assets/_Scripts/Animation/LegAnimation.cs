﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegAnimation : MonoBehaviour {
    public Transform Foot;
    public Transform FootTarget;
    public float minLegDistance = 1.0f;
    public float maxLegDistance = 3.0f;
    public AnimationCurve animationCurve;

    private bool isMoving = false;
    private float time;


    void Update () {
        if (Vector3.Distance(Foot.transform.position, FootTarget.transform.position) > Random.Range(minLegDistance, maxLegDistance)) {
            isMoving = true;
        }

        if (Foot.transform.position != FootTarget.transform.position && isMoving) {
            time += Time.deltaTime * 4;
            Vector3 pos = Vector3.Lerp(Foot.transform.position, FootTarget.transform.position, time);
            pos.y += animationCurve.Evaluate(time);
            Foot.transform.position = pos;
        }

        if (Foot.transform.position == FootTarget.transform.position) {
            isMoving = false;
            time = 0;
        }
    }
}