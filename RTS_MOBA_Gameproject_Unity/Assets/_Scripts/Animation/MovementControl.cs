﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementControl : MonoBehaviour {
    public float speed;


    void Update () {
        if (Input.GetAxis("Horizontal") < 0) {
            transform.Rotate(0.0f, -0.35f, 0.0f);
        }
        else if (Input.GetAxis("Horizontal") > 0) {
            transform.Rotate(0.0f, 0.35f, 0.0f);
        }

        if (Input.GetAxis("Vertical") < 0) {
            transform.position -= transform.forward * Time.deltaTime * speed;
        }
        else if (Input.GetAxis("Vertical") > 0) {
            transform.position += transform.forward * Time.deltaTime * speed;
        }
    }
}