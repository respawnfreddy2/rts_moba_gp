﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerSingleton : MonoBehaviour
{
    public static GameManagerSingleton Instance { get; private set; }
    public float disableEnemyDistanceRate = 30.0f;
    private GameObject player;
    public float timer = 2f;
    private float tikker;

    public int baseCaptured;
    public GameObject[] Walls;

    private void FixedUpdate()
    {
        if (baseCaptured < 4)
        {
            if (tikker >= timer)
            {
                controllAi();
                tikker = 0;
            }
            else
            {
                tikker += Time.deltaTime;
            }
        }
    }

    public void setActiveWalls(bool active)
    {
        if (active == false)
        {
            foreach (GameObject wall in Walls)
            {
                wall.SetActive(active);
            }

            StartCoroutine(ShowTooltip());
        }

        if (active == true)
        {
            foreach (GameObject spawner in GameObject.FindGameObjectsWithTag("Spawner"))
            {
                spawner.SetActive(false);
            }
            foreach (GameObject wall in Walls)
            {
                wall.SetActive(active);
            }
        }
    }

    private void controllAi()
    {
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if (enemy.transform.name != "PentaBoss")
            {
                if (enemy.GetComponent<StateController>())
                {
                    if (Vector3.Distance(player.transform.position, enemy.gameObject.transform.position) < disableEnemyDistanceRate)
                    {
                        enemy.GetComponent<StateController>().aiActive = true;
                    }
                    else
                    {
                        enemy.GetComponent<StateController>().aiActive = false;
                    }

                }
            }
        }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        player = GameObject.FindGameObjectWithTag("Player");
    }

    IEnumerator ShowTooltip()
    {
        var tooltip = GameObject.Find("PlayerInfos");

        if (tooltip)
            tooltip.GetComponent<Text>().text =
                "The Boss Arena is open!";

        yield return new WaitForSeconds(5);

        tooltip.GetComponent<Text>().text = " ";
    }
}