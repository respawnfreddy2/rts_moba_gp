﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossTriggerScript : MonoBehaviour
{
    public GameObject health;
    public Slider healhSlider;
    public StateController bossStats;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            health.SetActive(true);
            healhSlider.maxValue = bossStats.enemyStats.StartingHealth;
            healhSlider.value = bossStats.enemyStatsHandler.Health;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManagerSingleton.Instance.setActiveWalls(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            health.SetActive(false);
        }
    }
}
