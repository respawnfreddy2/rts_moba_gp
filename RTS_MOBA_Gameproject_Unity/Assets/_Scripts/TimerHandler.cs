﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerHandler : MonoBehaviour
{
    private float duration;
    private float durationCount;

    public TimerHandler(float duration)
    {
        this.duration = duration;
        this.durationCount = duration;
    }

    public bool isTimerFinish(float duration)
    {
        durationCount -= Time.deltaTime;

        if (durationCount <= 0)
        {
            durationCount = duration;
            return true;
        } else
            return false;
    }
}
