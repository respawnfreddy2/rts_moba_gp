﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skillmanager : MonoBehaviour
{
    #region Singleton
    public static Skillmanager instance;


    void Awake () 
    {
        instance = this;
    }
    #endregion

    public delegate void EventHandler (Skill skill);
    public event EventHandler OnNewSkill;
    

    public void updateSkill(Skill skill) {
        OnNewSkill(skill);
    }
}
