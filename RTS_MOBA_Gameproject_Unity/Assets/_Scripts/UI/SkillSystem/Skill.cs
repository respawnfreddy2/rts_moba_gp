﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Skill", menuName = "Skills/Skill")]
public class Skill : ScriptableObject
{
    public float damageModifier;
    public float ammuntionModifier;
    public float precisionModifier;

    public bool isAbility;

    public int price;
    public float fireRateModifier;

    public float healthModifier;

    public float utilityModifier;
    // Start is called before the first frame update
   public void Buy()
   {
       //purchase skin

   }

    // Update is called once per frame
    void Update()
    {
        
    }
}
