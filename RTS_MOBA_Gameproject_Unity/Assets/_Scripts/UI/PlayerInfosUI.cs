﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfosUI : MonoBehaviour
{
    private Text text;


    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }


    public void ShowInteractionInfo()
    {
        text.canvasRenderer.SetAlpha(1f);
        text.text =  "\n press E to interact";
        Invoke(nameof(FadeOut), 4);
    }

    public void ShowFlagInfo()
    {
        text.canvasRenderer.SetAlpha(1f);
        text.text =  "\n You have to deactivate the Towers before you can capture the flag";
        Invoke(nameof(FadeOut), 4);

    }


    private void FadeOut()
    {
        text.CrossFadeAlpha(0, 3, false);
    }
}