﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour
{
    public GameObject[] Huds;
    public GameObject deathPanel;
    [HideInInspector] public static bool playerIsDead = false;

    public void Update()
    {
        if (playerIsDead == false)
        {

            playerIsDead = false;
            deathPanel.SetActive(false);
        }   

        if (playerIsDead)
        {

                startDeathScreen();

        }
            
    }

    private void startDeathScreen()
    {
        Time.timeScale = 0f;
        deathPanel.SetActive(true); 
    }
}
