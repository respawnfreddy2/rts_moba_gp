﻿using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

namespace _Scripts.UI
{
    public class ProgressBar : MonoBehaviour
    {
        private int maximum;
        private int current;
        public Image Mask;
        public int Current
        {
            get => current;
            set
            {
                current = value;
                GetCurrentFill();
            }
        }
        void Start()
        {
            Current = 0;
            maximum = 100;
        }
        private void Update(){
        
        GetCurrentFill();
        }
        
        void GetCurrentFill()
        {
            float fillAmount = (float) current / (float) maximum;
            Mask.fillAmount = fillAmount;
        }
    }
}