﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using _Scripts.BuildingSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class ConstructionUI : MonoBehaviour
{
    // Start is called before the first frame update
    public TextMeshProUGUI TextField;
    public GameObject BuildingSystem;
    private BuildingController buildingController;
    private PriceList priceList;

    void Start()
    {
        buildingController = BuildingSystem.GetComponent<BuildingController>();
        priceList = BuildingSystem.GetComponent<PriceList>();
        try
        {
            //dirty af
        }
        catch (Exception e)
        {
            Debug.Log("ConstructionUI: couldn't find buildingController (" + e + ")");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!buildingController.InBuildingMode)
        {
            TextField.text = "(Tab)";
        }
        else
        {
            if (priceList == null)
                BuildingSystem.GetComponent<PriceList>();
                
            TextField.text = "\n\n";
            for (int i = 0; i < buildingController.AllBuildings.Length; i++)
            {
                TextField.text += "" + (i + 1) + ": " + buildingController.AllBuildings[i].name + " (" +
                                  priceList.GetPriceOf(buildingController.AllBuildings[i]) + ") \n";
            }
        }
    }
}