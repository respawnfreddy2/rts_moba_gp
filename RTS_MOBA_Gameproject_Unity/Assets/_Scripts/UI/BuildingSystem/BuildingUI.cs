﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using _Scripts.BuildingSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuildingUI : MonoBehaviour
{

    public GameObject BuildingSystem;
    private SelectBuilding selectBuilding;
    public Text TextField;
    
    
    // Start is called before the first frame update
    void Start()
    {
        selectBuilding = BuildingSystem.GetComponent<SelectBuilding>();
    }

    // Update is called once per frame
    void Update()
    {
        if (selectBuilding.selection != null)
            TextField.text = "Selected " + selectBuilding.selection.name;
        else
            TextField.text = "";
    }
}
