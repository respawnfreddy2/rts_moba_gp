﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class StatUI : MonoBehaviour
{
    public Transform skillPointText;
    public Transform levelText;

    public Transform moneyText;
    public PlayerStats playerStats;

    public GameObject WaterBarGameObject;
    public GameObject HealthBarGameObject;

    private TextMeshProUGUI WaterBarText;
    private TextMeshProUGUI HealthBarText;


    // Start is called before the first frame update
    void Start()
    {
        HealthBarText = HealthBarGameObject.GetComponent<TextMeshProUGUI>();
        WaterBarText = WaterBarGameObject.GetComponent<TextMeshProUGUI>();

        // skillPointText = transform.Find("SkillPoints");
        playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
        // levelText = transform.Find("Level");
        // moneyText = transform.Find("Money");
    }

    // Update is called once per frame
    void Update()
    {
        UpdateStatMonitor();
    }

    void UpdateStatMonitor()
    {
        WaterBarText.text = "Water: " + playerStats.currentWater + "/" + playerStats.MaxWater.GetValue();
        HealthBarText.text = "Health: " + playerStats.currentHealth + "/" + playerStats.MaxHealth.GetValue();



        // monitor ammunition and firemode
        //TODO is this low performant??
        Text money = moneyText.GetComponent<Text>();
        money.text = playerStats.Money.ToString() + "$";
        Text level = levelText.GetComponent<Text>();
        level.text = playerStats.level.ToString();
        Text points = skillPointText.GetComponent<Text>();
        points.text = playerStats.skillPoints.ToString() + " Skillpoints";
    }
}