﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningScreen : MonoBehaviour
{
    public GameObject[] Huds;
    public GameObject winningPanel;
    [HideInInspector] public static bool gameWon = false;

    public void Update()
    {
        if (gameWon)
            startWinScreen();
        else
        {
            winningPanel.SetActive(false);

        }

    }

    private void startWinScreen()
    {

        Time.timeScale = 0f;
        winningPanel.SetActive(true);
    }
}
