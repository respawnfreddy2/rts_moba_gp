﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
        public Transform textTr;
        public Weapon weapon;

    
    // Start is called before the first frame update
    void Start()
    {
        textTr = transform.Find("Text");
        weapon = gameObject.GetComponent<ShopEntry>().weapon;
        UpdateButtonText();
    }

    void UpdateButtonText()
    {
        Text text = textTr.GetComponent<Text>();
        text.text = weapon.name + "\n" + "\n" +
                    "Damage: " + weapon.damage + "\n" +
                    "Ammunition: " + weapon.ammunition + "\n" +
                    "Price: " + weapon.price + "\n";
    }
}