﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemManager : MonoBehaviour {
    #region Singleton


    public static ItemManager instance;

    void Awake () {
        instance = this;
    }
    #endregion
    // Start is called before the first frame update
    //TODO set primary weapon on start, then nullpointerexception should go away
    public Weapon defaultWeapon;
    public delegate void EventHandler (Weapon newItem);
    public event EventHandler OnWeaponChanged;

    public GameObject slot1;
    public GameObject slot2;
    public GameObject slot3;
    public GameObject slot4;



    //switch case?
    public void Equip (Weapon newItem) {
        if (newItem.isPrimary) {
            primaryWeapon = newItem;
            // GameObject.Find("Canvas/Ammunition").GetComponent<Image>().sprite = Instantiate(weaponImage).GetComponent<Image>().sprite;

            slot1.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = newItem.Icon;
        }
        else if (secondaryWeapon == null){
            secondaryWeapon = newItem;
            var tempcolour = slot2.GetComponent<Image>().color;
            tempcolour.a = 250f;
            slot2.GetComponent<Image>().color = tempcolour;
            slot2.transform.GetChild(0).gameObject.SetActive(true);
            slot2.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = newItem.Icon;


        }
        else if (thirdWeapon == null){
            thirdWeapon = newItem;
             var tempcolour = slot2.GetComponent<Image>().color;
            tempcolour.a = 250f;
            slot3.GetComponent<Image>().color = tempcolour;
            slot3.transform.GetChild(0).gameObject.SetActive(true);
            slot3.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = newItem.Icon;


        }
        else if(fourthWeapon == null){
             var tempcolour = slot2.GetComponent<Image>().color;
            tempcolour.a = 250f;
            slot4.GetComponent<Image>().color = tempcolour;
            slot4.transform.GetChild(0).gameObject.SetActive(true);
            fourthWeapon = newItem;
            slot4.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = newItem.Icon;


        }
            currentWeapon = newItem;
            OnWeaponChanged(newItem);
    }
    private Weapon currentWeapon;

    public Weapon primaryWeapon;
    public Weapon secondaryWeapon;

    public Weapon thirdWeapon;
    
    public Weapon fourthWeapon;
    public void AddShield (Shield shield) {

    }

    void Update()
    {
        if (Input.GetKeyDown("1") && primaryWeapon != null)
        {

            OnWeaponChanged(primaryWeapon);
            currentWeapon = primaryWeapon;


        } 
         if (Input.GetKeyDown("2")   && secondaryWeapon != null)
        {
            OnWeaponChanged(secondaryWeapon);
            currentWeapon = secondaryWeapon;
        } 
         if (Input.GetKeyDown("3")  && thirdWeapon != null && !currentWeapon.Equals(thirdWeapon))
        {
            {
            OnWeaponChanged(thirdWeapon);
            currentWeapon = thirdWeapon;
            }
        } 
         if (Input.GetKeyDown("4")  && fourthWeapon != null && !currentWeapon.Equals(fourthWeapon))
        {  
            OnWeaponChanged(fourthWeapon);
            currentWeapon = fourthWeapon;
        } 
    }
}
