﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Shop/ShopItem")]
public class ShopItem : ScriptableObject
{   
    public string itemName;
    public Sprite sprite;
    public int cost;

    public Stat[] referencedStats;

    public virtual void Buy(){
        //remove cost from money supply
    }
    public void Sell()
    {
        //Remove item and restore half of the cost
    }
    
  


}