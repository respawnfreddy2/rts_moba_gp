﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldEntry : MonoBehaviour
{
    public Shield shield;



    public Button button;
    void Start()
    {
        Button btn = button.GetComponent<Button>();
        btn.onClick.AddListener(onButtonPressed);
    }

    public void onButtonPressed()
    {
        ItemManager.instance.AddShield(shield);
        GameObject.Find("Player").GetComponent<PlayerController>().clipAmmunition = PlayerStats.instance.Ammunition.GetValue();
        if (PlayerStats.instance.Money - shield.price >= 0)
        {
            PlayerStats.instance.Money -= shield.price;
        }

    }

}

