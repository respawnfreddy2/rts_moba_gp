﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Inventory/Equipment")]
public class Weapon : ShopItem
{
    public float damage;
    public float ammunition;
    public float precisionModifier;

    public int price;
    public bool isPrimary;
    public float fireRateModifier;
    public Sprite Icon;


    public GameObject projectile;
    public GameObject weaponModel;
    // Start is called before the first frame update
   public override void Buy()
   {
       base.Buy();

   }

    // Update is called once per frame
    void Update()
    {
        
    }
}
