﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Shield", menuName = "Inventory/Shield")]
public class Shield : ShopItem
{
    
    public float armor;



    public int price;
    
    
    // Start is called before the first frame update
   public override void Buy()
   {
       base.Buy();

   }

    // Update is called once per frame
    void Update()
    {
        
    }
}
