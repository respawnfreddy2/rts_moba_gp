﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopEntry : MonoBehaviour
{
    public Weapon weapon;
    public PlayerStats playerStats;
    public GameObject projectile;
    public int Price;
    public Button button;
    public GameObject weaponModel;
    public Image weaponImage;


    void Start()
    {
        Button btn = button.GetComponent<Button>();
        btn.onClick.AddListener(onButtonPressed);
    }

    public void onButtonPressed()
    {
        if (PlayerStats.instance.Money - weapon.price >= 0)
        {
            PlayerStats.instance.Money -= weapon.price;
            ItemManager.instance.Equip(weapon);
            equipWeaponModel();
            GameObject.Find("Player").GetComponent<PlayerController>().clipAmmunition = PlayerStats.instance.Ammunition.GetValue();
            GameObject.Find("Player").GetComponent<PlayerController>().currentProjectile = this.projectile;
            button.interactable = false;
            button.GetComponent<Image>().color = Color.green;
        }
    }

    public void equipWeaponModel()
    {
        GameObject.Find("Player/Character_Prototype/Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand/Gun").GetComponent<MeshFilter>().sharedMesh = Instantiate(weaponModel).GetComponent<MeshFilter>().mesh;
        GameObject.Find("Canvas/Ammunition").GetComponent<Image>().sprite = Instantiate(weapon.Icon);
    }
}

