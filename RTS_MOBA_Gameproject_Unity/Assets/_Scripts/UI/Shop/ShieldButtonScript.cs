﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldButtonScript : MonoBehaviour
{
        public Transform textTr;
        public Shield shield;

    
    // Start is called before the first frame update
    void Start()
    {
    textTr = transform.Find("Text");
    shield = gameObject.GetComponent<ShieldEntry>().shield;
    UpdateButtonText();
    }


    void UpdateButtonText()
    {
        Text text = textTr.GetComponent<Text>();
        text.text = shield.name + "\n" + "\n" +
                    "Armor" + shield.armor + "\n" +
                    "Price: " + shield.price + "\n";
    }

    }