﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriceList : MonoBehaviour
{
    // Start is called before the first frame update

    public List<(string name, int price)> PriceListBuildings;

    void Start()
    {
        InitPriceList();
    }

    private void InitPriceList()
    {
        PriceListBuildings = new List<(string name, int price)>();
        PriceListBuildings.Add(("FriendlyTower", 30));
        PriceListBuildings.Add(("Labor", 60));
        PriceListBuildings.Add(("Shop", 40));
        PriceListBuildings.Add(("Wall", 5));
        PriceListBuildings.Add(("WaterPump", 30));
    }


    public bool CanBuyBuilding(GameObject building)
    {
        if (GetPriceOf(building) <= PlayerStats.instance.Money)
            return true;

        return false;
    }


    public void BuyBuilding(GameObject building)
    {
        if (GetPriceOf(building) <= PlayerStats.instance.Money)
            PlayerStats.instance.Money -= GetPriceOf(building);
    }

    public int GetPriceOf(GameObject gameObject)
    {
        foreach (var price in PriceListBuildings)
        {
            if (gameObject.name.Contains(price.name))
                return price.price;
        }
        return 100000;
    }
}