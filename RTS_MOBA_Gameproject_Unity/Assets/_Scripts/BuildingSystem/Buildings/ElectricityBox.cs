﻿using System.Collections;
using System.Collections.Generic;
using _Scripts.BuildingSystem;
using UnityEngine;

public class ElectricityBox : Building
{
    public float HP = 70;

    void Start()
    {
        
    }
    
    
    public void DeactivateTower()
    {
        var towerScript = GetComponentInParent<Tower>();
        if (towerScript != null)
        {
            SoundManager.PlaySound("TowerDeactivate");
            towerScript.isActivated = false;      
            towerScript.lineDrawer.SetPosition(0, towerScript.FirePoint.transform.position);
            towerScript.lineDrawer.SetPosition(1, towerScript.FirePoint.transform.position);
            
        }
    }

    public void doDamage(float amount)
    {
        HP -= amount;
        if (HP < 0)
        {
            DeactivateTower();
            SpawnFracturedBox();            
        }
    }

    private void SpawnFracturedBox()
    {
        var preFabFracturedBox = Resources.Load("BuildingSystem/FracturedBuildings/electricalBoxFracturedNew");

        if (preFabFracturedBox != null)
        {
            
            GameObject temp = (GameObject) Instantiate(preFabFracturedBox,
                gameObject.transform.position, transform.rotation,
                transform.parent);
            temp.transform.RotateAround(temp.transform.position, transform.right, 90);
            // temp.transform.RotateAround(temp.transform.position, transform.forward, 180);
            
            Destroy(temp, 3.5f);
        }


        foreach (Transform child in transform.parent)
        {
            if(child.name.Equals("Cable"))
                Destroy(child.gameObject);
        }
        
        
        
        Destroy(gameObject);
    }
    
    
}