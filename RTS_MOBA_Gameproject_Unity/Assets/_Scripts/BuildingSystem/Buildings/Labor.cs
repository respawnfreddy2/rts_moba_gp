﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts.BuildingSystem;
using UnityEngine;

public class Labor : Building
{
    private LineRenderer lineRenderer;
    private ParticleSystem laserParticle;


    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponentInChildren<LineRenderer>();
        lineRenderer.SetPosition(0, new Vector3(0, 0, 0));
        lineRenderer.SetPosition(1, new Vector3(0, 0, 0));
        
        laserParticle = GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
            StartLaser();

        if (Input.GetKeyDown(KeyCode.H))
            ReturnLaser();
    }

    private void StartLaser()
    {
        Vector3 targetPos = lineRenderer.GetPosition(1);
        targetPos.z += 30;
        StartCoroutine(MoveLaser(lineRenderer, targetPos, 2));
    }

    private void ReturnLaser()
    {
        Vector3 targetPos = lineRenderer.GetPosition(1);
        targetPos.z -= 30;
        StartCoroutine(MoveLaser(lineRenderer, targetPos, 2));
    }


    public IEnumerator MoveLaser(LineRenderer lineRenderer, Vector3 position, float timeToMove)
    {
        var currentPos = lineRenderer.GetPosition(1);
        var t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            lineRenderer.SetPosition(1, Vector3.Lerp(currentPos, position, t));
            yield return null;
        }
    }

    public override void Interact()
    {
        StartLaser();
        laserParticle.Play();
    }
}