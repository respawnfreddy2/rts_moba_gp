﻿using _Scripts.UI;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.UI;


namespace _Scripts.BuildingSystem
{
    public class FlagScript : MonoBehaviour
    {
        private float distance;
        private float captureProgress = 0;
        private Collider[] playersNearby;
        private Collider[] EnemiesNearby;
        private bool canBeCaptured;
        private float rangeBoarderRange;

        public bool Captured = false;

        public ProgressBar progressBar;

        [Header("FlagStuff")] public GameObject Flag;
        public ParticleSystem RangeBorder;
        public Material TeamOneMaterial;
        public Gradient TeamOneGradient;
        public int Range = 10;
        public int RangeAfterCapture = 20;
        public Tower[] towers;
        public EnemySpawner EnemySpawner;


        void Start()
        {
            distance = 20;
            canBeCaptured = false;
            playersNearby = new Collider[10];
            EnemiesNearby = new Collider[10];
            progressBar.Current = 0;
        }

        // Update is called once per frame
        void Update()
        {
            if (!canBeCaptured)
            {
                SetDistance();
                CheckIfCanBeCaptured();
            }
            else
            {
                SetDistance();
                MakeCaptureProgress();
            }
        }




        private void MakeCaptureProgress()
        {
            int enemiesNearbyCount = Physics.OverlapSphereNonAlloc(transform.position, Range, EnemiesNearby, 1 << 15);

            if (distance <= Range + 5 && !Captured && enemiesNearbyCount == 0)
            {
                RangeBorder.gameObject.SetActive(true);
                RangeBorder.Play();

                captureProgress += 20 * Time.deltaTime;
                if (captureProgress >= 100)
                {
                    GameManagerSingleton.Instance.baseCaptured += 1;
                    if (GameManagerSingleton.Instance.baseCaptured >= 4)
                    {
                        GameManagerSingleton.Instance.setActiveWalls(false);
                    }
                    CaptureFlag();
                }

                RangeBorder.Stop();
                
                
                
                if (progressBar.Current != (int) captureProgress)
                    progressBar.Current = (int) captureProgress;
            }
            else if (!Captured && distance >= Range + 5  && distance <= Range + 20)
            {
                progressBar.Current = 0;
            }
    

        }


        private void CaptureFlag()
        {
            //change the range particle system
            RangeBorder.gameObject.SetActive(false);
            ParticleSystem.MainModule psMain = RangeBorder.main;
            psMain.startColor = TeamOneGradient;
            var sh = RangeBorder.shape;
            sh.radius = RangeAfterCapture;
            RangeBorder.gameObject.SetActive(true);

            if (EnemySpawner != null)
                EnemySpawner.isSpawning = false;
            
            captureProgress = 0;
            Captured = true;
            Flag.transform.GetComponent<Renderer>().material = TeamOneMaterial;

            DeleteTowers();
            InitLootBoxes();
        }

        private void InitLootBoxes()
        {
            GameObject lootBox = Resources.Load<GameObject>("BuildingSystem/LootBoxes/LootBoxAnimated");
            GameObject tempSpawn = GameObject.FindGameObjectWithTag("BuildingSystem");


            if (tempSpawn != null && lootBox != null)
            {
                foreach (Transform child in transform)
                {
                    if (child.name.Equals("LootBoxPlaces"))
                        foreach (Transform lootBoxPositions in child)
                        {
                            //GameObject tempBox = Instantiate(lootBox, lootBoxPositions);
                            tempSpawn.GetComponent<SpawnScript>().SpawnLootBox(lootBoxPositions.transform.position, lootBox);
                        }
                }
            }
        }



        
        
        private void DeleteTowers()
        {
            //spawn foundations 
            foreach (var tower in towers)
            {
                var preFabFoundation = Resources.Load("BuildingSystem/Foundations/Foundation");

                if (preFabFoundation != null)
                {
                    foreach (Transform child in tower.gameObject.transform)
                    {
                        if (child.CompareTag("Foundation"))
                        {
                            Instantiate(preFabFoundation, child.transform.position,
                                Quaternion.Euler(new Vector3(270, 180, 0)),
                                transform.parent);
                        }
                    }
                }
            }


            //spawn fractured tower
            foreach (var tower in towers)
            {
                var preFabFracturedTower = Resources.Load("BuildingSystem/FracturedBuildings/TowerFractured");

                if (preFabFracturedTower != null)
                {
                    Destroy(tower.gameObject);

                    SoundManager.PlaySound("TowerFalling");

                    GameObject temp = (GameObject) Instantiate(preFabFracturedTower,
                        tower.gameObject.transform.position, tower.transform.rotation,
                        transform.parent);

                    foreach (Transform child in temp.transform)
                    {
                        Material mat = child.gameObject.GetComponent<MeshRenderer>().material;
                        Color newColor = mat.color;
                        newColor.a = 0.0f;
                        mat.color = newColor;
                        child.gameObject.GetComponent<MeshRenderer>().material = mat;
                    }

                    Destroy(temp, 3.5f);
                }
            }
        }

        private void SetDistance()
        {
            int playersNearbyCount = Physics.OverlapSphereNonAlloc(transform.position, Range, playersNearby, 1 << 11);

            if (playersNearbyCount != 0)
                distance = Vector3.Distance(transform.position, playersNearby[0].gameObject.transform.position);
            else
                distance = 20;
        }

        private void CheckIfCanBeCaptured()
        {
            foreach (var towerScript in towers)
            {
                if (towerScript.isActivated)
                {
                    return;
                }
            }

            EnemySpawner.SpawnRate = EnemySpawner.SpawnRate * 6;
            canBeCaptured = true;
        }
    }
}