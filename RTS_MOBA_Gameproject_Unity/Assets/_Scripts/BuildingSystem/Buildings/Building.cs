﻿using UnityEngine;

namespace _Scripts.BuildingSystem
{
    public class Building : MonoBehaviour
    {
        public bool isActivated = false;

        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
        }

        public virtual void Interact()
        {
        }
    }
}