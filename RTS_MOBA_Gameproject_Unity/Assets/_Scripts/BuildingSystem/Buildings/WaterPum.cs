﻿using System.Collections;
using System.Collections.Generic;
using _Scripts.BuildingSystem;
using UnityEngine;

public class WaterPum : Building
{
    public int MaxWater = 100;
    public int CurrentWater = 0;

    [HideInInspector] public GameObject player;
    private PlayerStats playerStats;

    private float distance;
    public float range;

    private new ParticleSystem particleSystem;
    private MoveParticleToTarget moveParticle;

    private float lastWaterSoundTime;


    // Start is called before the first frame update
    void Start()
    {
        lastWaterSoundTime = Time.time;
        lastWaterSoundTime -= 5;
        particleSystem = GetComponentInChildren<ParticleSystem>();
        moveParticle = particleSystem.GetComponent<MoveParticleToTarget>();

        distance = 100;
        CurrentWater = +30;
        player = GameObject.FindWithTag("Player");

        if (player != null)
        {
            playerStats = player.GetComponent<PlayerStats>();
        }

        StartCoroutine(IncreaseWater());
    }

    // Update is called once per frame
    void Update()
    {
        if (isActivated)
        {
            distance = Vector3.Distance(player.transform.position, transform.position);

            if (distance < range)
                GivePlayerWater();
        }
    }

    private void GivePlayerWater()
    {
        if (playerStats.currentWater < playerStats.MaxWater.GetValue())
        {
            if (Time.time - lastWaterSoundTime > 5f)
            {
                SoundManager.PlaySound("GetWater");
                lastWaterSoundTime = Time.time;
            }


            moveParticle.StartParticleEffect(player.transform);

            playerStats.currentWater += CurrentWater;

            if (playerStats.currentWater > playerStats.MaxWater.GetValue())
                playerStats.currentWater = playerStats.MaxWater.GetValue();

            CurrentWater = 0;
        }
    }


    IEnumerator IncreaseWater()
    {
        while (true)
        {
            if (CurrentWater < MaxWater)
            {
                CurrentWater += 1;
                yield return new WaitForSeconds(0.2f);
            }
            else
            {
                yield return null;
            }
        }
    }
}