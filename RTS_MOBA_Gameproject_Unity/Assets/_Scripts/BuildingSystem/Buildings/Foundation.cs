﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foundation : MonoBehaviour
{
    public bool hasSomethingOnIt;

    // Start is called before the first frame update
    void Start()
    {
        hasSomethingOnIt = false;
        Invoke(nameof(DisableBoxColliderTrigger),3.5f);
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void DisableBoxColliderTrigger()
    {

        if (this.gameObject.TryGetComponent(out BoxCollider boxCollider))
        {
            boxCollider.isTrigger = false;
        }
        
    }
}