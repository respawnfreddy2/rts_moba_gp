﻿using System.Collections;
using System.Collections.Generic;
using _Scripts.BuildingSystem;
using _Scripts.BuildingSystem.TowerShoots;
using _Scripts.VFX;
using UnityEngine;

public class Tower : Building
{
    public GameObject FirePoint;
    public List<GameObject> Vfx = new List<GameObject>();
    private GameObject effectToSpawn;

    public RotateProjectile RotateProjectile;
    private Collider[] playersNearby;
    private double timeToFire = 0;
    [HideInInspector] public LineRenderer lineDrawer;


    void Start()
    {
        isActivated = true;
        effectToSpawn = Vfx[0];
        playersNearby = new Collider[5];
        lineDrawer = GetComponentInChildren<LineRenderer>();
        if (lineDrawer != null)
        {
            lineDrawer.SetPosition(0, FirePoint.transform.position);
            lineDrawer.SetPosition(1, FirePoint.transform.position);
        }
    }

    void Update()
    {
        if (isActivated)
        {
            int playersNearbyCount = Physics.OverlapSphereNonAlloc(transform.position, 9f, playersNearby, 1 << 11);

            if (playersNearbyCount > 0)
            {
                RotateProjectile.RotateGun(playersNearby[0].gameObject);

                if (playersNearby[0].gameObject.name.Equals("MeleeCollider"))
                    lineDrawer.SetPosition(1, playersNearby[0].transform.parent.gameObject.transform.position);
                else
                    lineDrawer.SetPosition(1, playersNearby[0].gameObject.transform.position);


                if (Time.time >= timeToFire)
                {
                    timeToFire = Time.time + 1 / effectToSpawn.GetComponent<ProjectileController>().fireRate;
                    SpawnVfx();
                }
            }
            else
            {
                lineDrawer.SetPosition(1, FirePoint.transform.position);
            }
        }
    }

    private void SpawnVfx()
    {
        GameObject vfx;

        if (FirePoint != null)
        {
            RotateProjectile.RotateToGameObject(playersNearby[0].gameObject);
            vfx = Instantiate(effectToSpawn, FirePoint.transform.position, Quaternion.identity);
            if (RotateProjectile != null)
            {
                vfx.transform.localRotation = RotateProjectile.GetRotation();
            }
        }
    }
}