﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using _Scripts.VFX;
using UnityEngine;

public class ProjectileSpeed : ProjectileController
{


    // Start is called before the first frame update
    void Start()
    {
        if (flashEffect != null)
        {
            var shootVfx = Instantiate(flashEffect, transform.position, Quaternion.identity);
            shootVfx.transform.forward = gameObject.transform.forward;

            var psShoot = shootVfx.GetComponent<ParticleSystem>();
            if (psShoot != null)
                Destroy(shootVfx, psShoot.main.duration);
            else
            {
                var psChild = shootVfx.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(shootVfx, psChild.main.duration);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (projectileSpeed != 0)
            transform.position += transform.forward * (projectileSpeed * Time.deltaTime);
    }

  


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.name == "MeleeCollider") return;

        if (other.gameObject.CompareTag("Barrel")) {
            other.transform.GetComponent<Barrel>().DoDamage(PlayerStats.instance.Damage.GetValue());
        }

        if (other.gameObject.CompareTag("Enemy")) {
            other.gameObject.GetComponent<EnemyStatsHandler>().DoDamage(damage);
            projectileSpeed = 0;
        }
        if (other.gameObject.CompareTag("ElectricalBox"))
        {
            other.gameObject.GetComponent<ElectricityBox>().doDamage((PlayerStats.instance.Damage.GetValue()));
        }
        
        
        

        if (bulletHit != null)
        {
            var hitVfx = Instantiate(bulletHit, transform.position, transform.rotation);

            var psHit = hitVfx.GetComponent<ParticleSystem>();
            if (psHit != null)
                Destroy(hitVfx, psHit.main.duration);
            else
            {
                var psChild = hitVfx.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitVfx, psChild.main.duration);
            }
        }
        Destroy(gameObject);
    }
    
    
    
}