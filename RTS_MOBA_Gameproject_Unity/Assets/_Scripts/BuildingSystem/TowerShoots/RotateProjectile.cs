﻿using UnityEngine;

namespace _Scripts.BuildingSystem.TowerShoots
{
    public class RotateProjectile : MonoBehaviour
    {
        // public Camera Camera;
        // public float MaximumLength;
        private Ray rayMouse;
        private Vector3 pos;
        private Vector3 direction;
        private Quaternion rotation;


        public void RotateToGameObject(GameObject obj)
        {
            direction = obj.transform.position - transform.position;
            rotation = Quaternion.LookRotation(direction);

        }

        public void RotateGun(GameObject obj)
        {
            transform.LookAt(2 * transform.position - obj.transform.position);
            transform.GetChild(0).transform.LookAt(2 * transform.position - obj.transform.position);
        }
        
        


        void RotateToMouseDirection(GameObject obj, Vector3 destination)
        {
            direction = destination - obj.transform.position;
            rotation = Quaternion.LookRotation(direction);
            obj.transform.localRotation = Quaternion.Lerp(obj.transform.rotation, rotation, 1);
        }

        public Quaternion GetRotation()
        {
            return rotation;
        }
    }
}