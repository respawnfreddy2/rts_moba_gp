﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts.BuildingSystem;
using _Scripts.BuildingSystem.TowerShoots;
using _Scripts.VFX;
using UnityEngine;

public class FriendlyTower : Building
{
    public GameObject FirePoint;
    public List<GameObject> Vfx = new List<GameObject>();
    public RotateProjectile RotateProjectile;
    public int Range;

    private GameObject effectToSpawn;
    private Collider[] EnemiesNerby;
    private double timeToFire = 0;

    private LineRenderer lineDrawer;

 

    void Start()
    {
        Range = 10;
        effectToSpawn = Vfx[0];
        EnemiesNerby = new Collider[5];


        lineDrawer = GetComponentInChildren<LineRenderer>();
        lineDrawer.SetPosition(0, FirePoint.transform.position);
        lineDrawer.SetPosition(1, FirePoint.transform.position);
    }

    void Update()
    {
        if (isActivated)
        {
            int enemiesNearbyCount = Physics.OverlapSphereNonAlloc(transform.position, Range, EnemiesNerby, 1 << 15);

            if (enemiesNearbyCount > 0)
            {
                RotateProjectile.RotateGun(EnemiesNerby[0].gameObject);
                lineDrawer.SetPosition(0, FirePoint.transform.position);
                lineDrawer.SetPosition(1,EnemiesNerby[0].transform.position);

                if (Time.time >= timeToFire)
                {
                    timeToFire = Time.time + 1 / effectToSpawn.GetComponent<ProjectileController>().fireRate;
                    SpawnVfx();
                }
            }
            else
            {
                lineDrawer.SetPosition(1, FirePoint.transform.transform.position);
            }
        }
    }

    private void SpawnVfx()
    {
        GameObject vfx;

        if (FirePoint != null)
        {
            RotateProjectile.RotateToGameObject(EnemiesNerby[0].gameObject);

            vfx = Instantiate(effectToSpawn, FirePoint.transform.position, Quaternion.identity);
            if (RotateProjectile != null)
            {
                vfx.transform.localRotation = RotateProjectile.GetRotation();
            }
        }
    }
}