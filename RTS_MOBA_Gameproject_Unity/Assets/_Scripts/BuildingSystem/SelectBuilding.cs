﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace _Scripts.BuildingSystem
{
    public class SelectBuilding : MonoBehaviour
    {
        public BuildingController BuildingController;

        public Material highlightMaterial;

        public LayerMask mask;


        [FormerlySerializedAs("Selection")] [HideInInspector]
        public Transform selection;

        private Material defaultMatForSelection;
        private List<Material> defaultMatForChildren;

        public GameObject selectionUi;
        private Image buildingBackground;
        private Text buildingText;

        // Start is called before the first frame update
        void Start()
        {
            buildingBackground = selectionUi.GetComponentInChildren<Image>();
            buildingText = selectionUi.GetComponentInChildren<Text>();
            buildingBackground.enabled = false;

            defaultMatForChildren = new List<Material>();
        }

        // Update is called once per frame
        void Update()
        {
            if (BuildingController.InBuildingMode && selection != null)
                ResetSelection();

            if (Input.GetMouseButtonDown(0) && !Input.GetMouseButton(1) && !BuildingController.InBuildingMode)
            {
                ResetSelection();

                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
                {
                    if (hit.collider.gameObject.layer == 12)
                    {
                        SelectFlag(hit);
                    }
                    else
                    {
                        SelectOther(hit);
                    }

                    SetSelectionText();
                }
            }
        }

        private void SetSelectionText()
        {
            buildingBackground.enabled = true;

            if (selection != null)
                buildingText.text = "Selected " + selection.name;
            else
                buildingText.text = "";
        }

        private void ResetSelection()
        {
            if (selection != null)
            {
                buildingText.text = "";
                buildingBackground.enabled = false;

                if (selection.gameObject.layer == 12)
                    ResetFlag();
                else
                {
                    var selectionRenderer = selection.GetComponent<Renderer>();
                    selectionRenderer.material = defaultMatForSelection;
                    selection = null;
                }
            }
        }

        private void ResetFlag()
        {
            for (int i = 0; i < selection.transform.childCount; i++)
            {
                if (selection.transform.GetChild(i).GetComponent<ParticleSystem>() != null)
                {
                    selection.transform.GetChild(i).gameObject.SetActive(false);
                    i++;
                }
                else
                {
                    if (selection.transform.GetChild(i).TryGetComponent(out Renderer rend))
                        rend.material = defaultMatForChildren[i];
                    // if (selection.transform.GetChild(i).GetComponent<Renderer>() != null)
                    //     selection.transform.GetChild(i).GetComponent<Renderer>().material = defaultMatForChildren[i];
                }
            }

            defaultMatForChildren.Clear();
            selection = null;
        }

        private void SelectFlag(RaycastHit hit)
        {
            foreach (Transform child in hit.transform)
            {
                if (child.GetComponent<ParticleSystem>() != null)
                {
                    child.gameObject.SetActive(true);
                    child.GetComponent<ParticleSystem>().Play();
                }
                else
                {
                    if (child.GetComponent<Renderer>() != null)
                    {
                        var tempRenderer = child.GetComponent<Renderer>();
                        defaultMatForChildren.Add(tempRenderer.material);
                        tempRenderer.material = highlightMaterial;
                    }
                }
            }

            selection = hit.transform;
        }

        private void SelectOther(RaycastHit hit)
        {
            var tempSelection = hit.transform;
            var tempSelectionRenderer = tempSelection.GetComponent<Renderer>();

            if (tempSelectionRenderer != null)
            {
                defaultMatForSelection = tempSelectionRenderer.material;
                tempSelectionRenderer.material = highlightMaterial;
            }

            selection = tempSelection;
        }
    }
}