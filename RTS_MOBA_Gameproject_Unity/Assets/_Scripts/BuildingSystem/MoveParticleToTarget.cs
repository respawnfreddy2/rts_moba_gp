﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveParticleToTarget : MonoBehaviour
{
    private Transform target;

    private ParticleSystem system;

    private static ParticleSystem.Particle[] particles = new ParticleSystem.Particle[1000];

    int count;

    private float startTime;

    private bool startedParticle = false;


    void Start()
    {
        target = GameObject.Find("Player").transform;

        startTime = Time.time;
        if (system == null)
            system = GetComponent<ParticleSystem>();

        if (system == null)
        {
            this.enabled = false;
        }
        else
        {
        }
    }

    void Update()
    {
        if (Time.time - startTime > 2 && startedParticle)
        {
            MoveParticle();
        }
    }


    public void StartParticleEffect(Transform player)
    {
        target = player;

        if (Time.time - startTime > 5)
        {
            system.Clear();
            startTime = Time.time;
        }

        system.Play();
        startedParticle = true;
    }

    private void MoveParticle()
    {
        count = system.GetParticles(particles);

        for (int i = 0; i < count; i++)
        {
            ParticleSystem.Particle particle = particles[i];

            Vector3 v1 = system.transform.TransformPoint(particle.position);
            Vector3 v2 = target.transform.position;

            Vector3 tarPosi = (v2 - v1) * (particle.remainingLifetime / particle.startLifetime);
            particle.position = system.transform.InverseTransformPoint(v2 - tarPosi);

            particles[i] = particle;
        }

        system.SetParticles(particles, count);
    }
}