﻿using System.Collections;
using System.Collections.Generic;
using _Scripts.BuildingSystem;
using _Scripts.BuildingSystem.MaterialSetterS;
using UnityEngine;
using UnityEngine.AI;

public class SpawnScript : MonoBehaviour
{
    private List<( Vector3 end, GameObject gameObject)> buildingsToMove;

    [HideInInspector]
    public PriceList priceList;


    void Start()
    {
        buildingsToMove = new List<( Vector3 end, GameObject gameObject)>();
        priceList = gameObject.GetComponent<PriceList>();
    }


    // Update is called once per frame
    void Update()
    {
        if (buildingsToMove.Count > 0)
        {
            for (int i = 0; i < buildingsToMove.Count; i++)
            {
                Vector3 RV = buildingsToMove[i].end - buildingsToMove[i].gameObject.transform.position;
                buildingsToMove[i].gameObject.transform.Translate(RV * Time.deltaTime, Space.World);

                if (buildingsToMove[i].gameObject.transform.position.y >= buildingsToMove[i].end.y - 0.2)
                {
                    buildingsToMove.RemoveAt(i);
                }
            }
        }
    }

    public void PlaceBuilding(Vector3 position, Quaternion rotation, GameObject building, Collider[] foundationsNearby)
    {
        if (building.GetComponent<MaterialSetter>().CanBePlaced && priceList.CanBuyBuilding(building))
        {
            //decrease the money
            priceList.BuyBuilding(building);

            //set the scripts and disable foundation for the next building
            SetScriptsActive(true, building);
            
            Destroy(foundationsNearby[0].gameObject);

            //set the material to default and delete the material setter
            GameObject tempGameObject = Instantiate(building, position, rotation);
            tempGameObject.transform.SetParent(transform);
            tempGameObject.GetComponent<Collider>().isTrigger = false;
            MaterialSetter tempMaterialSetter = tempGameObject.GetComponent<MaterialSetter>();
            tempMaterialSetter.SetMaterial(tempMaterialSetter.MatDefault);
            Destroy(tempMaterialSetter);

            //deactivate the scripts of the template 
            SetScriptsActive(false, building);

            AddBuildingToMoveList(tempGameObject);
        }
    }

    public void SpawnLootBox(Vector3 position, GameObject lootBox)
    {
         GameObject tempGameObject = Instantiate(lootBox, position, Quaternion.identity);
        AddBuildingToMoveList(tempGameObject);
    }


    private void AddBuildingToMoveList(GameObject tempGameObject)
    {
        Vector3 endPos = tempGameObject.transform.position;

        Vector3 startPos = tempGameObject.transform.position;
        startPos.y -= 5;
        tempGameObject.transform.position = startPos;

        buildingsToMove.Add((endPos, tempGameObject));

    }


    private void SetScriptsActive(bool activated, GameObject building)
    {
        if (building.CompareTag("Tower"))
        {
            building.GetComponentInChildren<LineRenderer>().enabled = activated;
        }

        building.GetComponent<Building>().isActivated = activated;
      //  building.AddComponent<NavMeshObstacle>();
    }
}