﻿using System;
using UnityEngine;

namespace _Scripts.BuildingSystem.MaterialSetterS
{
    public class MaterialSetter : MonoBehaviour
    {
        public Material MatCanPlace, MatPlacingFailed, MatDefault;
        public Renderer Rend;

        public Collider[] flagsNearby;


        public bool CanBePlaced = false;
        public bool IsOnAFoundation;


        private void Awake()
        {
            IsOnAFoundation = false;
            SetMaterial(MatPlacingFailed);

            flagsNearby = new Collider[10];
        }

        void Start()
        {
            Rend = GetComponent<Renderer>();
            Rend.enabled = true;
        }


        // Update is called once per frame
        public void FixedUpdate()
        {
            int flagsNearbyCount = Physics.OverlapSphereNonAlloc(transform.position, 20f, flagsNearby, 1 << 12);
            
            if (flagsNearbyCount >= 1)
            {
                if (flagsNearby[0].GetComponent<FlagScript>().Captured)
                {

                    
                    if (!IsOnAFoundation)
                    {
                        SetMaterial(MatPlacingFailed);
                        CanBePlaced = false;

                    }
                    else if (IsOnAFoundation)
                    {

                        SetMaterial(MatCanPlace);
                        CanBePlaced = true;
                    }
                }

            }
            else
            {
                SetMaterial(MatPlacingFailed);
                CanBePlaced = false;
            }
        }
        
        

        public virtual void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Foundation") &&
                !other.gameObject.GetComponent<Foundation>().hasSomethingOnIt)
            {
                IsOnAFoundation = true;
            }else if (other.gameObject.layer != 8)
            {
                IsOnAFoundation = false;
            }
        }

      
        

        public virtual void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Foundation"))
            {
                IsOnAFoundation = false;
            }
        }


        public void SetMaterial(Material material)
        {
            Rend.sharedMaterial = material;
            foreach (Transform child in transform)
            {
                if (child.name.Equals("LineDrawer") || child.GetComponent<ParticleSystem>() != null)
                    continue;


                var tempRend = child.GetComponent<Renderer>();
                if (tempRend != null)
                    tempRend.material = material;
            }
        }
    }
}