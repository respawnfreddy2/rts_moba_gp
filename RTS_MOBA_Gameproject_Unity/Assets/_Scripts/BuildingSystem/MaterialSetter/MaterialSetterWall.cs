﻿using System.Collections;
using System.Collections.Generic;
using _Scripts.BuildingSystem;
using _Scripts.BuildingSystem.MaterialSetterS;
using UnityEngine;

public class MaterialSetterWall : MaterialSetter
{
    // Start is called before the first frame update

    public int objectCount;

    void Start()
    {
        objectCount = 0;
    }

    public new void FixedUpdate()
    {
        int flagsNearbyCount = Physics.OverlapSphereNonAlloc(transform.position, 11f, flagsNearby, 1 << 12);

        if (flagsNearbyCount >= 1)
        {
            if (flagsNearby[0].GetComponent<FlagScript>().Captured)
            {
                if (objectCount >= 1)
                {
                    SetMaterial(MatPlacingFailed);
                    CanBePlaced = false;
                }

                else if (objectCount < 1)
                {
                    SetMaterial(MatCanPlace);
                    CanBePlaced = true;
                }
            }
        }
        else
        {
            SetMaterial(MatPlacingFailed);
            CanBePlaced = false;
        }
    }


    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != 8 && !other.gameObject.CompareTag("Wall"))
            objectCount++;
    }

    public new void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != 8)
            objectCount--;
    }
}