﻿using _Scripts.BuildingSystem.MaterialSetterS;
using UnityEngine;

namespace _Scripts.BuildingSystem
{
    public class MaterialSetterWaterPump : MaterialSetter
    {
        private void Awake()
        {
            IsOnAFoundation = false;
            flagsNearby = new Collider[10];
            SetMaterial(MatPlacingFailed);
        }

        public new void FixedUpdate()
        {
            base.FixedUpdate();
        }


        public override void OnTriggerEnter(Collider other)
        {
            
            if (other.gameObject.CompareTag("WaterFoundation") &&
                !other.gameObject.GetComponent<Foundation>().hasSomethingOnIt)
            {
                IsOnAFoundation = true;
            }else if (other.gameObject.layer != 8)
            {
                IsOnAFoundation = false;
            }
        }

        public override void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer == 17)
            {
                IsOnAFoundation = false;
            }
        }
    }
}