﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Scripts.BuildingSystem.MaterialSetterS;
using UnityEngine;

public class WallPlacer : MonoBehaviour
{
    // Start is called before the first frame update
    public bool SetTheStartOfTheWall = false;
    public Vector3 StartOfTheWall;
    public Vector3 EndOfTheWall;
    public List<GameObject> AllPartsOfTheWall;

    private GameObject templateWall;


    void Start()
    {
        AllPartsOfTheWall = new List<GameObject>();
    }

    public void WallBuilding(GameObject currentSelectedBuilding)
    {
        //set the start of the Wall
        if (!SetTheStartOfTheWall && Input.GetMouseButtonDown(0))
        {
            templateWall = currentSelectedBuilding;

            if (AllPartsOfTheWall != null && AllPartsOfTheWall.Count > 0)
                foreach (var wallPart in AllPartsOfTheWall)
                    Destroy(wallPart);

            AllPartsOfTheWall = new List<GameObject>();
            StartOfTheWall = currentSelectedBuilding.transform.position;
            SetTheStartOfTheWall = true;
        }
        //end the wall
        else if (Input.GetMouseButtonDown(0))
        {
            SetTheWall(currentSelectedBuilding);
        }
        //update wall, as long as the end point has not been set
        else if (SetTheStartOfTheWall)
        {
            UpdateWall(currentSelectedBuilding);
        }
    }

    private void UpdateWall(GameObject CurrentSelectedBuilding)
    {
        List<(Vector3 position, float rotation)> points =
            GetAllPosBetweenPoints(StartOfTheWall, CurrentSelectedBuilding.transform.position);

        bool shouldReload = false;

        if (points.Count == AllPartsOfTheWall.Count)
        {
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].position != AllPartsOfTheWall[i].transform.position)
                {
                    shouldReload = true;
                    break;
                }
            }
        }


        if (shouldReload || points.Count != AllPartsOfTheWall.Count)
        {
            if (AllPartsOfTheWall.Count > 0)
            {
                foreach (var part in AllPartsOfTheWall)
                    Destroy(part);
                AllPartsOfTheWall.Clear();
            }

            foreach (var point in points)
            {
                GameObject tempWallPart = Instantiate(CurrentSelectedBuilding, point.position,
                    Quaternion.Euler(new Vector3(-90, point.rotation, 0)));

                // tempWallPart.transform.Rotate(new Vector3(0, 0, point.rotation), Space.Self);
                tempWallPart.transform.SetParent(transform);
                AllPartsOfTheWall.Add(tempWallPart);
            }

            if (AllPartsOfTheWall.Count > 0)
                CurrentSelectedBuilding.transform.rotation = AllPartsOfTheWall.Last().transform.rotation;
        }
    }

    private void SetTheWall(GameObject currentSelectedBuilding)
    {
        if (AllPartsOfTheWall.Last().GetComponent<MaterialSetter>().CanBePlaced)
            PlaceBuilding(currentSelectedBuilding.transform.position, AllPartsOfTheWall.Last().transform.rotation,
                currentSelectedBuilding);

        foreach (var wallPart in AllPartsOfTheWall)
        {
            if (wallPart.GetComponent<MaterialSetter>().CanBePlaced)
            {
                wallPart.GetComponent<BoxCollider>().isTrigger = false;
                MaterialSetter tempMaterialSetter = wallPart.GetComponent<MaterialSetter>();
                tempMaterialSetter.SetMaterial(tempMaterialSetter.MatDefault);
                Destroy(tempMaterialSetter);
            }
            else
            {
                Destroy(wallPart);
            }
        }

        AllPartsOfTheWall.Clear();
        SetTheStartOfTheWall = false;
    }

    public void ResetTheWall()
    {
        foreach (var wallPart in AllPartsOfTheWall)
        {
            Destroy(wallPart);
        }

        SetTheStartOfTheWall = false;
    }


    private List<(Vector3 position, float rotation)> GetAllPosBetweenPoints(Vector3 sta, Vector3 end)
    {
        Vector3 start = sta;

        List<(Vector3 position, float rotation)> allPoints = new List<(Vector3 position, float rotation)>
            {(start, 45)};


        while (!start.Equals(end))
        {

            float rotation = 0;

            if (Math.Abs(start.x - end.x) > 0.2 &&
                Math.Abs(start.z - end.z) > 0.2)
            {
                if (start.x > end.x)
                {
                    start.x--;
                    if (start.z > end.z)
                    {
                        rotation = 90 + 45;
                        start.z--;
                    }
                    else
                    {
                        rotation = 180 + 45;
                        start.z++;
                    }
                }
                else
                {
                    start.x++;
                    if (start.z > end.z)
                    {
                        rotation = 180 + 45;
                        start.z--;
                    }
                    else
                    {
                        rotation = 90 + 45;
                        start.z++;
                    }
                }
            }
            else if (Math.Abs(start.x - end.x) > 0.2)
            {
                if (start.x > end.x)
                    start.x--;
                else
                    start.x++;

                rotation = 180;
            }
            else if (Math.Abs(start.z - end.z) > 0.2)
            {
                if (start.z > end.z)
                    start.z--;
                else
                    start.z++;

                rotation = 90;
            }
            {
                var newPoint = start;
                allPoints.Add((newPoint, rotation));
            }

            if (allPoints.Count > 50)
                break;
        }

        //destroy some points

        for (int i = 0; i < allPoints.Count; i++)
        {
            if (i != 0 && i % 4 != 0)
            {
                allPoints.RemoveAt(i);
            }
        }
        
        //destroy some points if its not diagonal
        for (int i = 0; i < allPoints.Count - 1; i++)
        {
            if (Math.Abs(allPoints[i].position.x - allPoints[i + 1].position.x) > 0.2 &&
                Math.Abs(allPoints[i].position.z - allPoints[i + 1].position.z) > 0.2)
            {
                
            }else if (Math.Abs(allPoints[i].position.x - allPoints[i + 1].position.x) > 0.2 || Math.Abs(allPoints[i].position.z - allPoints[i + 1].position.z) > 0.2)
            {
                if (i % 4 != 0 && i != 0)
                    allPoints.RemoveAt(i);

            }
        }


        // //sets the rotation of the second obj to the first (tuple list is immutable, so you have to create a new one)

        if (allPoints.Count >= 2)
        {
            var newFirst = allPoints.ElementAt(0);
            newFirst.rotation = allPoints.ElementAt(1).rotation;
            allPoints.RemoveAt(0);
            allPoints.Insert(0, newFirst);
        }

        //delete the last one for the cursor
        if (allPoints.Count > 0)
            allPoints.RemoveAt(allPoints.Count - 1);
        return allPoints;
    }

    private void PlaceBuilding(Vector3 position, Quaternion rotation, GameObject building)
    {
        if (building.GetComponent<MaterialSetter>().CanBePlaced)
        {
            //set the material to default and delete the material setter
            GameObject tempGameObject = Instantiate(building, position, rotation);
            tempGameObject.transform.SetParent(transform);
            tempGameObject.GetComponent<BoxCollider>().isTrigger = false;
            MaterialSetter tempMaterialSetter = tempGameObject.GetComponent<MaterialSetter>();
            tempMaterialSetter.SetMaterial(tempMaterialSetter.MatDefault);
            Destroy(tempMaterialSetter);
        }
    }
}