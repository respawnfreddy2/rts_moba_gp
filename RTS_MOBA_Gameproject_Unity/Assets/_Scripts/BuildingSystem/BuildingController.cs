﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using _Scripts.BuildingSystem.MaterialSetterS;
using UnityEditor;
using UnityEngine;

namespace _Scripts.BuildingSystem
{
    public class BuildingController : MonoBehaviour
    {
        public bool InBuildingMode = false;
        private bool hasChosenABuilding;
        public GameObject[] AllBuildings;
        private GameObject currentSelectedBuilding;

        public LayerMask Mask;
        private int laserPosX, laserPosY, laserPosZ = 0;
        private Vector3 mousePos;
        private WallPlacer wallPlacer;
        private Collider[] foundationsNearby;

        [HideInInspector] public SpawnScript spawnScript;
        [HideInInspector] public PriceList priceList;
        


        private void Start()
        {
            priceList = gameObject.GetComponent<PriceList>();
            spawnScript = gameObject.AddComponent<SpawnScript>();

            foundationsNearby = new Collider[5];
            wallPlacer = gameObject.AddComponent<WallPlacer>();
            AllBuildings = Resources.LoadAll<GameObject>("BuildingSystem/BuildingToChoose");
        }

        // Update is called once per frame
        void Update()
        {
            ActivateConstructionMode();
            if (!InBuildingMode)
                return;
            ChooseBuilding();
            if (!hasChosenABuilding)
                return;

            UpdateCursor();

            MakeBuilding();
        }

        private void UpdateCursor()
        {
            mousePos = Input.mousePosition;
            Ray ray = Camera.main.ScreenPointToRay(mousePos);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, Mask))
            {
                int posX = (int) Mathf.Round(hit.point.x);
                int posZ = (int) Mathf.Round(hit.point.z);

                if (posX != laserPosX || posZ != laserPosZ)
                {
                    laserPosX = posX;
                    laserPosZ = posZ;

                    if (currentSelectedBuilding.CompareTag("Wall"))
                        SetCursorForWall(hit, posX, posZ);
                    else if (currentSelectedBuilding.CompareTag("WaterPump"))
                        SetCursorForWaterPump(hit, posX, posZ);
                    else
                        SetCursorForBuilding(hit, posX, posZ);
                }
            }
        }

        private void SetCursorForWall(RaycastHit hit, int posX, int posZ)
        {
            Vector3 colliderSize = currentSelectedBuilding.GetComponent<MeshRenderer>().bounds.extents;
            currentSelectedBuilding.transform.position = new Vector3(posX, 0, posZ);
        }

        private void SetCursorForBuilding(RaycastHit hit, int posX, int posZ)
        {
            int foundationsCount;
            if (currentSelectedBuilding.CompareTag("WaterPump"))
                foundationsCount = Physics.OverlapSphereNonAlloc(hit.point, 3f, foundationsNearby, 1 << 17);
            else
                foundationsCount = Physics.OverlapSphereNonAlloc(hit.point, 3f, foundationsNearby, 1 << 13);


            if (foundationsCount > 0)
            {
                var foundationScript = foundationsNearby[0].gameObject.GetComponent<Foundation>();

                if (!foundationScript.hasSomethingOnIt)
                {
                    SetBuildingOnFoundation();
                }
                else
                    currentSelectedBuilding.transform.position = new Vector3(posX, 0, posZ);
            }
            else
            {
                currentSelectedBuilding.transform.position = new Vector3(posX, 0, posZ);
                MakeSpecificOffsets();
            }
        }

        private void SetBuildingOnFoundation()
        {
            if (currentSelectedBuilding.CompareTag("Tower"))
                currentSelectedBuilding.transform.position = new Vector3(
                    foundationsNearby[0].gameObject.transform.position.x, -0.5f,
                    foundationsNearby[0].gameObject.transform.position.z);
            else if (currentSelectedBuilding.CompareTag("Shop"))
            {
                currentSelectedBuilding.transform.position = new Vector3(
                    foundationsNearby[0].gameObject.transform.position.x, -0.2f,
                    foundationsNearby[0].gameObject.transform.position.z);
            }
            else
                currentSelectedBuilding.transform.position = foundationsNearby[0].gameObject.transform.position;
        }

        private void MakeSpecificOffsets()
        {
            if (currentSelectedBuilding.CompareTag("Shop"))
            {
                Vector3 newPos = currentSelectedBuilding.transform.position;
                newPos.y -= 0.5f;
                currentSelectedBuilding.transform.position = newPos;
            }
        }

        private void SetCursorForWaterPump(RaycastHit hit, int posX, int posZ)
        {
            int resourcesCount = Physics.OverlapSphereNonAlloc(hit.point, 3f, foundationsNearby, 1 << 17);

            if (resourcesCount > 0)
            {
                var tempPos = foundationsNearby[0].gameObject.transform.position;
                tempPos.y = 1.5f;
                currentSelectedBuilding.transform.position = tempPos;
            }
            else
            {
                currentSelectedBuilding.transform.position = new Vector3(posX, 1.4f, posZ);
            }
        }

        private void MakeBuilding()
        {
            if (hasChosenABuilding)
                if (currentSelectedBuilding.name.Contains("Wall"))
                    wallPlacer.WallBuilding(currentSelectedBuilding);
                else
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        spawnScript.PlaceBuilding(currentSelectedBuilding.transform.position,
                            currentSelectedBuilding.transform.rotation, currentSelectedBuilding, foundationsNearby);
                        Destroy(currentSelectedBuilding);
                        hasChosenABuilding = false;
                    }
                }
        }

        private void ChooseBuilding()
        {
            //TODO:: check the input for number and make it automatic
            if (Input.GetKeyDown("1"))
            {
                Destroy(currentSelectedBuilding);
                currentSelectedBuilding = Instantiate(AllBuildings[0], mousePos, AllBuildings[0].transform.rotation);
                currentSelectedBuilding.transform.SetParent(transform);
                hasChosenABuilding = true;
            }
            else if (Input.GetKeyDown("2"))
            {
                Destroy(currentSelectedBuilding);
                currentSelectedBuilding = Instantiate(AllBuildings[1], mousePos, AllBuildings[1].transform.rotation);
                currentSelectedBuilding.transform.SetParent(transform);
                hasChosenABuilding = true;
            }
            else if (Input.GetKeyDown("3"))
            {
                Destroy(currentSelectedBuilding);
                currentSelectedBuilding = Instantiate(AllBuildings[2], mousePos, AllBuildings[2].transform.rotation);
                currentSelectedBuilding.transform.SetParent(transform);
                hasChosenABuilding = true;
            }
            else if (Input.GetKeyDown("4"))
            {
                Destroy(currentSelectedBuilding);
                currentSelectedBuilding = Instantiate(AllBuildings[3], mousePos, AllBuildings[3].transform.rotation);
                currentSelectedBuilding.transform.SetParent(transform);
                hasChosenABuilding = true;
            }

            else if (Input.GetKeyDown("5"))
            {
                Destroy(currentSelectedBuilding);
                currentSelectedBuilding = Instantiate(AllBuildings[4], mousePos, AllBuildings[4].transform.rotation);
                currentSelectedBuilding.transform.SetParent(transform);
                hasChosenABuilding = true;
            }


            //destroy the wall if another building has been chosen
            if (wallPlacer.SetTheStartOfTheWall &&
                (Input.GetKeyDown("1") || Input.GetKeyDown("2") || Input.GetKeyDown("3")))
            {
                wallPlacer.ResetTheWall();
            }
        }

        private void ActivateConstructionMode()
        {
            if (Input.GetButtonDown("Build"))
            {
                if (InBuildingMode)
                {
                    if (wallPlacer.SetTheStartOfTheWall)
                        wallPlacer.ResetTheWall();

                    Destroy(currentSelectedBuilding);
                    hasChosenABuilding = false;
                }

                InBuildingMode = !InBuildingMode;
            }

            if (Input.GetMouseButtonDown(1) && InBuildingMode)
            {
                if (wallPlacer.SetTheStartOfTheWall)
                    wallPlacer.ResetTheWall();

                Destroy(currentSelectedBuilding);
                hasChosenABuilding = false;
                InBuildingMode = false;
            }
        }
    }
}