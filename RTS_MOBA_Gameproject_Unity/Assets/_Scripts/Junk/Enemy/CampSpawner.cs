﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampSpawner : MonoBehaviour
{
    public GameObject[] EnemysTypesList;
    [Range(0, 300)]
    public float RespawnTimer;
    public Vector3 SpawnSize;
    public int CampSize;
    [HideInInspector]
    public int CurrentCampSize;
    public bool SwarmBehavior = false;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = transform.parent.position;
        CurrentCampSize = 0;
        SpawnEnemies();
    }

    public void Update()
    {
        SpawnEnemies();
    }

    public void SpawnEnemies()
    {
        if (CurrentCampSize == 0)
        {
            StartCoroutine(StartRespawnTimer());

            print("Campsize: " + CurrentCampSize);
            for (int i = 0; i <= CampSize; i++)
            {
                foreach (var enemy in EnemysTypesList)
                {
                    Vector3 pos = transform.parent.localPosition + new Vector3(Random.Range(-SpawnSize.x / 2, SpawnSize.x / 2), 1 ,Random.Range(-SpawnSize.z / 2, SpawnSize.z / 2));
                    var e = Instantiate(enemy, pos, Quaternion.identity);
                    e.GetComponent<StatHandler>().campSpawner = this.gameObject;
                    e.transform.parent = this.transform.parent.GetChild(1).transform;
                    CurrentCampSize++;
                }
            }
        }
    }

    public IEnumerator StartRespawnTimer()
    {
        print("Start Respawn Timer");
        while (true)
        {
            yield return new WaitForSeconds(RespawnTimer);
            // CurrentCampSize = CampSize;
            // SpawnEnemies();
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0.5f, 0.3f);
        Gizmos.DrawCube(transform.parent.localPosition, SpawnSize);
    }

}
