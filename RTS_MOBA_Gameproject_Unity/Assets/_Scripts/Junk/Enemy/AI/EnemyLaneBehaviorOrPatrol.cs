﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyLaneBehaviorOrPatrol : MonoBehaviour
{
    public Transform[] destinations;
    public float lookRadius;
    public float waitTimeOnPatrolPoint;
    public NavMeshAgent navMeshAgent;
    SphereCollider SphereCollider;

    bool follow;
    bool enemyInRange;

    private int currentDestinationPointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        SphereCollider = GetComponent<SphereCollider>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        SphereCollider.radius = lookRadius;
    }

    // Update is called once per frame
    void Update()
    {
        if (navMeshAgent.remainingDistance < 2f)
        {
            print("Next Patrollpoint");
            MoveToNextDestination();
        }
    }

    public void Awake()
    {
        MoveToNextDestination();
    }

    public void MoveToNextDestination()
    {
        if (destinations.Length > 0)
        {
            navMeshAgent.SetDestination(destinations[currentDestinationPointIndex].position);
            currentDestinationPointIndex++;
            currentDestinationPointIndex %= destinations.Length;
        }
    }
}
