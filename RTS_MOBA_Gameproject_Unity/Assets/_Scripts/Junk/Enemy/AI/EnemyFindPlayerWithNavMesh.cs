﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyFindPlayerWithNavMesh : MonoBehaviour
{

    public List<GameObject> Destinations = new List<GameObject>();
    public float lookRadius;
    NavMeshAgent navMeshAgent;
    SphereCollider SphereCollider;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        SphereCollider = GetComponent<SphereCollider>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        SphereCollider.radius = lookRadius;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<StatHandler>().isAttacking && Vector3.Distance(FindClosestPlayer().position, transform.position) != 0)
        {
            float distance = Vector3.Distance(FindClosestPlayer().position, transform.position);

            if (distance <= lookRadius)
            {
                navMeshAgent.SetDestination(FindClosestPlayer().position);
                animator.SetBool("walking", true);

                if (distance <= navMeshAgent.stoppingDistance)
                {
                    // Attack the target

                    // Face the target
                    FaceTarget();
                }
            }
            else
            {
                this.GetComponent<StatHandler>().isAttacking = false;
                animator.SetBool("walking", false);
            }
        }
    }

    void FaceTarget()
    {
        Vector3 direction = (FindClosestPlayer().position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    public Transform FindClosestPlayer()
    {
        float distance = 0;
        GameObject gameObject = null;

        foreach (var p in Destinations)
        {
            if (distance != 0)
            {
                if (distance >= Vector3.Distance(p.transform.position, transform.position))
                {
                    distance = Vector3.Distance(p.transform.position, transform.position);
                    gameObject = p;
                }
            }
            else
            {
                distance = Vector3.Distance(p.transform.position, transform.position);
                gameObject = p;
            }
        }

        Transform d = null;
        if (gameObject != null)
        {
            d = gameObject.transform;
        }
        
        // Destroy(gameObject);
        return d;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            print(other.gameObject);
            Destinations.Add(other.gameObject);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Destinations.Remove(other.gameObject);
        }
    }
}
