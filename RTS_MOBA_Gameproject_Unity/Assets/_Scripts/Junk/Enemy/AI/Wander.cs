﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Wander : MonoBehaviour
{
    public bool isWandering;
    public GameObject[] wps;
    NavMeshAgent agent;
    

    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        //int d = Random.Range(0, wps.Length);
        //agent.SetDestination(wps[d].transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (agent.remainingDistance < 2 && isWandering == true)
        {
            // go somewhere else random
            int d = Random.Range(0, wps.Length);
            agent.SetDestination(wps[d].transform.position);
        } 
        else if (agent.hasPath)
        {
            Vector3 toTarget = agent.steeringTarget - this.transform.position;
            float turnAngle = Vector3.Angle(this.transform.forward, toTarget);
            agent.acceleration = turnAngle * agent.speed / 3;
        }
    }
}
