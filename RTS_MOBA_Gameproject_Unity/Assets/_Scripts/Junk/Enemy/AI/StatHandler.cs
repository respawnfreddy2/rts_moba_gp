﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

public class StatHandler : MonoBehaviour
{
    public new GameObject gameObject;
    [HideInInspector]
    public float Health;
    [HideInInspector]
    public float Energy;
    [HideInInspector]
    public bool isAttacking = false;
    [HideInInspector]
    public GameObject campSpawner;

    public float StartingHealth;
    public float StartingEnergy;

    [Tooltip("It add everytime 1.0 Health if it's out of fight")]
    [Range(0, 5)]
    public float healrateSeconds;

    [Tooltip("It add evertime 1.0 Mana if it's out of fight")]
    [Range(0, 5)]
    public float energyRateSeconds;

    void Start()
    {
        Health = StartingHealth;
        Energy = StartingEnergy;
        // Gaining Mana and Health only when isAttackingCD is false
        StartCoroutine(AddHealth());
        StartCoroutine(AddEnergy());
    }

    public void GetDamage(float damageAmount, Transform dTransform)
    {
        //The Return is here to get a Reward for Damage other Enemies or make a Kill
     
        if (Health > 0)
        {
            isAttacking = true;
            Health -= damageAmount;
            print("Enemy bekommt: " + damageAmount + " Schaden!\nEnemy currentHealth: " + Health);

            if (gameObject.transform.parent.parent.GetComponentInChildren<CampSpawner>().SwarmBehavior == true)
            {
                foreach (var e in gameObject.transform.parent.parent.GetComponentsInChildren<StatHandler>())
                {
                    e.isAttacking = true;
                }
            }

            if (Health <= 0)
            {
                print("Enemy dead");
                campSpawner.GetComponent<CampSpawner>().CurrentCampSize--;
                Destroy(this.gameObject);
                dTransform.GetComponent<PlayerStats>().GainEP(2.0f);
            }
        }
    }

    IEnumerator AddHealth()
    {
        while (true)
        { // loops forever...
            if (Health < StartingHealth && !isAttacking)
            { // if health < 100...
                Health += 1; // increase health and wait the specified time
                yield return new WaitForSeconds(healrateSeconds);
            }
            else
            { // if health >= 100, just yield 
                yield return null;
            }
        }
    }

    IEnumerator AddEnergy()
    {
        while (true)
        { // loops forever...
            if (Energy < StartingEnergy && !isAttacking)
            { // if mana < 100...
                Energy += 1; // increase health and wait the specified time
                yield return new WaitForSeconds(energyRateSeconds);
            }
            else
            { // if health >= 100, just yield 
                yield return null;
            }
        }
    }
}