﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour {
    public Transform textTr;
    public Skill skill;
    public PlayerStats playerStats;
    public Button button;
    public int skillCount = 0;

    public bool isUltimateSkill;

    public Button childSkill;

    public int maxCount = 3;

    public Button ultimateSkill;


    // Start is called before the first frame update
    // maybe disable the button when its not clickable


    void UpdateButtonText () {
        Text text = textTr.GetComponent<Text>();
        text.text = skill.name + skillCount.ToString() + "/" + maxCount.ToString();
    }
    void Start () {
        textTr = transform.Find("Text");
        Button btn = button.GetComponent<Button>();
        btn.onClick.AddListener(onButtonPressed);
        UpdateButtonText();
        if(isUltimateSkill)
        {
            maxCount = 1;
        }
    }

    public void onButtonPressed () {
        if (PlayerStats.instance.skillPoints - 1 >= 0 && skillCount < maxCount) {
            if (isUltimateSkill == true && childSkill.GetComponent<SkillButton>().maxCount > childSkill.GetComponent<SkillButton>().skillCount) {
                return;
            }

            PlayerStats.instance.skillPoints -= 1;
            Skillmanager.instance.updateSkill(skill);
            skillCount++;
            UpdateButtonText();
            if(skillCount == maxCount)
            {
                GetComponent<Image>().color= Color.green;
                GetComponent<Button>().interactable = false;
                if(isUltimateSkill == false){
                    ultimateSkill.interactable = true;
                }
            }
        }
    }
}