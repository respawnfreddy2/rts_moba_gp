﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SkillPointsMeter : MonoBehaviour
{
    public Transform textTr;
    public PlayerStats playerStats;

    // Start is called before the first frame update
    void Start()
    {
        textTr = transform.Find("SkillPointCounter");
        playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateStatMonitor();
    }

    void UpdateStatMonitor()
    {
        
        
        // monitor ammunition and firemode

        Text text = textTr.GetComponent<Text>();
        text.text = "Skillpoints " + playerStats.skillPoints;
    }
}