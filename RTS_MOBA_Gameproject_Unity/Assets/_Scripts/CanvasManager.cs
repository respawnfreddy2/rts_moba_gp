﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
    public static CanvasManager Instance { get; private set; }
    public Animator flashPanel;

    public void flash()
    {
        flashPanel.SetTrigger("flashTrigger");
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
      public void UpdateWeaponMonitor()
    {
        // monitor ammunition and firemode
        //Transform textTr = GameObject.Find("Canvas").transform.Find("VarMonitor");
        //Text text = textTr.GetComponent<Text>();
        //text.text =  GameObject.Find("Player").GetComponent<PlayerController>().clipAmmunition + "/" + PlayerStats.instance.ammunitionSupply;
    }
}

