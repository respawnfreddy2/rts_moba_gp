﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DynamicObstacle : MonoBehaviour
{
    NavMeshObstacle nMO;

    // Start is called before the first frame update
    void DoNavMeshObstacle()
    {
        nMO = this.gameObject.AddComponent<NavMeshObstacle>();

        if (this.gameObject.GetComponent<BoxCollider>() != null)
        {
            print("hallo" +
                "test");
    
            nMO.center = this.GetComponent<BoxCollider>().center;
            nMO.size = this.GetComponent<BoxCollider>().size;

        } else if (this.gameObject.GetComponent<CapsuleCollider>() != null)
        {
            nMO.shape = NavMeshObstacleShape.Capsule;
            nMO.center = this.GetComponent<CapsuleCollider>().center;
            nMO.radius = this.GetComponent<CapsuleCollider>().radius;
            nMO.height = this.GetComponent<CapsuleCollider>().height;
        }

        nMO.carving = true;
    }
}
