﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Scripts.BuildingSystem;
using _Scripts.VFX;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour
{
    public Animator animator;
    public GameObject ejectionEffect;
    public float moveSpeed;
    public GameObject Shop;
    public GameObject SkillSystem;
    public float clipAmmunition;
    public float inaccuracy;
    public bool useController;
    public bool makeMeleeDamage;
    [HideInInspector] public PauseMenu pauseMenu;
    public CameraShake cameraShake;

    public GameObject vfxContainer;
    private GameObject crossHair;
    private Rigidbody characterRigidbody;
    private Vector3 moveDirection = Vector3.zero;
    private Transform firePoint;
    private Transform ejectionPoint;
    private GameObject knifeHand;
    private GameObject knifeBack;
    private bool isAlive = true;
    private bool isAiming = false;
    private bool isDiving = false;
    private bool isMoving = false;
    private bool isShooting = false;
    private bool isMeleeing = false;
    private bool isReloading = false;
    private bool weaponAutoFire = true;
    private float lastShot;
    private float playerMoveSpeed;
    public float lastRocket;
    private float cooldown;

    public PlayerInfosUI playerInfoUI;
    public List<GameObject> shootingProjectiles;
    public List<GameObject> abilities;
    [HideInInspector] public GameObject currentProjectile;

    [Header("Interaction")] public ParticleSystem interactionEffect;
    private float lastInteractionEffectTime;
    public float effectRepeatingTime;
    private Collider[] interactableObjectsNearby;
    private Collider nearestInteractBuilding;
    private int interactableCount;
    public float interactRadius = 2f;

    private float lastStepTime;


    void Start()
    {
        // setting up game variables 
        characterRigidbody = GetComponent<Rigidbody>();
        ejectionPoint =
            transform.Find(
                "Character_Prototype/Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand/Gun/EjectionPoint");
        firePoint = transform.Find(
            "Character_Prototype/Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand/Gun/FirePoint");
        knifeHand = GameObject.Find(
            "Character_Prototype/Player/Hip/Waist/Chest/L.Shoulder/L.UpArm/L.LoArm/L.Hand/KnifeHand");
        knifeBack = GameObject.Find("Character_Prototype/Player/Hip/Waist/KnifeBack");
        crossHair = GameObject.Find("Crosshair");
        playerMoveSpeed = GetComponent<PlayerStats>().MovementSpeed.GetValue();
        knifeHand.SetActive(false);
        crossHair.SetActive(false);
        pauseMenu = GameObject.Find("Canvas").GetComponent<PauseMenu>();

        if (GameObject.Find("vfx_Container"))
        {
            vfxContainer = GameObject.Find("vfx_Container");
        }
        

        CanvasManager.Instance.UpdateWeaponMonitor();

        interactableObjectsNearby = new Collider[10];
        lastInteractionEffectTime = Time.time;
        currentProjectile = shootingProjectiles[0];
        StartCoroutine(ShowTooltip());

        lastStepTime = Time.time;
    }

    void Update()
    {
        // setting up detection loops
        if (isAlive)
        {
            UpdateInteractableObjects();
            PlayerControls();
        }

        if (transform.position.y < -3)
        {
            isAlive = false;
            PlayerStats.instance.GetDamage(1000);
        }
    }

    void FixedUpdate()
    {
        if (isMoving && isAlive)
        {
            characterRigidbody.MovePosition(characterRigidbody.position +
                                            moveDirection * moveSpeed * Time.fixedDeltaTime);
        }
    }

    private void PlayerControls()
    {
        // right mouse click detection
        if ((Input.GetButton("Aim") || (Input.GetAxis("RHorizontal") != 0 || Input.GetAxis("RVertical") != 0)) &&
            !isDiving)
        {
            Aiming();
        }
        else
        {
            isAiming = false;
            animator.SetBool("aiming", false);
            moveSpeed = playerMoveSpeed;
            crossHair.SetActive(false);
        }

        // WASD or left stick detection
        if ((Input.GetAxisRaw("Horizontal") != 0 || (Input.GetAxisRaw("Vertical") != 0)))
        {
            isMoving = true;
            Running();
        }
        else
        {
            animator.SetBool("running", false);
            isMoving = false;
        }

        //Hotkey to cheat EP
        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    PlayerStats.instance.GainEP(20);
        //}

        //Hotkey to cheat Gears
        //if (Input.GetKeyDown(KeyCode.V))
        //{
        //    PlayerStats.instance.Money += 100;
        //}

        // input detection for reloading
        if (Input.GetButtonDown("Reload") && !isReloading && PlayerStats.instance.ammunitionSupply >= 0)
        {
            SoundManager.PlaySound("Reload");
            isReloading = true;
            Reloading();
            animator.SetTrigger("reload");
        }

        // input detection for interact
        if (Input.GetButtonDown("Interact"))
            Interact();

        if (Input.GetButtonDown("Start"))
        {
            if (pauseMenu.gameIsPaused)
            {
                pauseMenu.Resume();
            }
            else
            {
                pauseMenu.Pause();

                PlayerController player = GetComponent<PlayerController>();

                if (player.useController)
                {
                    Button btn = GameObject.Find("ResumeButton").GetComponent<Button>();
                    btn.Select();
                }
            }
        }
    }

    public void TriggerReloadAnimation()
    {
        if (!isReloading && PlayerStats.instance.ammunitionSupply >= 0)
        {
            isReloading = true;
            Reloading();
            animator.SetTrigger("reload");
        }
    }


    private void Running()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        Vector3 inputAngle = Vector3.right * x + Vector3.forward * y;

        moveDirection = Vector3.zero;
        moveDirection.x = Input.GetAxis("Horizontal");
        moveDirection.z = Input.GetAxis("Vertical");

        // move the characters rigidbody
        animator.SetBool("running", true);

        // translate the animator parameters for walking
        animator.SetFloat("X", moveDirection.x);
        animator.SetFloat("Y", moveDirection.z);

        //sound
        if (!isDiving)
            CheckWalkSounds();



        // look towards walking direction when not aiming
        if (!isAiming)
        {
            transform.rotation = Quaternion.LookRotation(inputAngle, Vector3.up);

            if (cooldown <= 0)
            {
                // input detection for the dodge roll
                if ((Input.GetButtonDown("Dive") || Input.GetAxis("DiveAxis") != 0) && !isDiving && !isReloading)
                {
                    SoundManager.PlaySound("Roll");
                    isDiving = true;
                    StartCoroutine(Diving());
                }
            }
            else
            {
                cooldown -= Time.deltaTime;
            }
        }
    }

    private void CheckWalkSounds()
    {
        if (isAiming)
        {
            if (Time.time - lastStepTime > 0.7)
            {
                SoundManager.PlaySound("Steps");
                lastStepTime = Time.time;
            }
        }
        else
        {
            if (Time.time - lastStepTime > 0.4)
            {
                SoundManager.PlaySound("Steps");
                lastStepTime = Time.time;
            }
        }
    }

    private void Aiming()
    {
        moveSpeed = 2;
        isAiming = true;
        crossHair.SetActive(true);

        if (Input.GetAxis("RHorizontal") != 0 || Input.GetAxis("RVertical") != 0)
        {
            // aiming with gamepad
            float x = Input.GetAxis("RHorizontal");
            float y = Input.GetAxis("RVertical");

            Vector3 aimAngle = Vector3.right * y + Vector3.forward * -x;
            transform.rotation = Quaternion.LookRotation(aimAngle, Vector3.up);
        }
        else
        {
            Camera cam = GameObject.Find("MainCamera").GetComponent<Camera>();

            // aiming with Mouse
            Ray cameraRay = cam.ScreenPointToRay(Input.mousePosition);
            Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
            float rayLength;

            if (groundPlane.Raycast(cameraRay, out rayLength))
            {
                Vector3 pointToLook = cameraRay.GetPoint(rayLength);
                Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue);

                transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));

                crossHair.transform.position = pointToLook;
            }
        }

        // set aiming animation
        if (!isReloading)
        {
            animator.SetBool("aiming", true);
        }

        // input detection for shooting
        if (weaponAutoFire)
        {
            if (Input.GetButton("Shoot") || Input.GetAxis("ShootAxis") != 0)
            {
                Shooting();
            }
        }

        else
        {
            if ((Input.GetButtonDown("Shoot") || Input.GetAxis("ShootAxis") != 0) && !isShooting)
            {
                isShooting = true;
                Shooting();
            }

            if (Input.GetAxis("ShootAxis") < 0.1)
            {
                isShooting = false;
            }
        }

        // input detection for melee attack
        if ((Input.GetButtonDown("Dive") || Input.GetAxis("DiveAxis") != 0) && !isMeleeing && !isReloading)
        {
            isMeleeing = true;
            MeleeAttack();
        }

        if ((Input.GetButton("Skill") && PlayerStats.instance.rockets))
        {

            Cast();
        }
    }

    private void Shooting()
    {
        if (Time.time > currentProjectile.GetComponent<ProjectileController>().fireRate + lastShot &&
            clipAmmunition > 0 &&
            !isReloading)
        {
            // trigger shooting effects
            if (firePoint != null)
            {
                GameObject vfx;
                GameObject vfx3;
                float randomX = Random.Range(-inaccuracy, inaccuracy);
                float randomY = Random.Range(-inaccuracy, inaccuracy);

                vfx = Instantiate(currentProjectile, firePoint.transform.position, firePoint.transform.rotation,
                    vfxContainer.transform);
                vfx.transform.Rotate(randomX, randomY, 0.0f);
                currentProjectile.GetComponent<ProjectileController>().damage =
                    PlayerStats.instance.Damage.GetValue();


                SoundManager.PlaySound(currentProjectile.name);


                //TODO:: revert to one and clean up this shit
                Destroy(vfx, 10);

                vfx3 = Instantiate(ejectionEffect, ejectionPoint.transform.position, Quaternion.identity,
                    vfxContainer.transform);
                Rigidbody rb = vfx3.GetComponent<Rigidbody>();
                rb.AddRelativeForce(ejectionPoint.transform.forward * 16);
                Destroy(vfx3, 5);

                animator.SetTrigger("shooting");
                if(currentProjectile.name == "ProjectileShotgun")
                {
                    clipAmmunition -= 5;
                    PlayerStats.instance.ammunitionSupply -= 5;

                }
                else
                {
                    clipAmmunition--;
                     PlayerStats.instance.ammunitionSupply--;

               }
               

                StartCoroutine(cameraShake.ScreenShake(0.15f, 0.02f));
            }


            lastShot = Time.time;
        }

        // automatic reload detection
        if (clipAmmunition == 0 && !isReloading && PlayerStats.instance.ammunitionSupply > 0)
        {
            SoundManager.PlaySound("Reload");
            isReloading = true;
            animator.SetTrigger("reload");
            Reloading();
        }

        CanvasManager.Instance.UpdateWeaponMonitor();
    }

    //TODO write switch case 
    private void Cast()
    {
        if (Time.time > 20 + lastRocket)
        {
            // trigger shooting effects
            if (firePoint != null)
            {
                GameObject vfx;
                GameObject vfx3;
                float randomX = Random.Range(-inaccuracy, inaccuracy);
                float randomY = Random.Range(-inaccuracy, inaccuracy);

                vfx = Instantiate(abilities[0], firePoint.transform.position, firePoint.transform.rotation,
                    vfxContainer.transform);
                vfx.transform.Rotate(randomX, randomY, 0.0f);

                SoundManager.PlaySound("RocketLauncher");

                //TODO:: revert to one
                Destroy(vfx, 10);

                vfx3 = Instantiate(ejectionEffect, ejectionPoint.transform.position, Quaternion.identity,
                    vfxContainer.transform);
                Rigidbody rb = vfx3.GetComponent<Rigidbody>();
                rb.AddRelativeForce(ejectionPoint.transform.forward * 16);
                Destroy(vfx3, 5);

                animator.SetTrigger("shooting");

                StartCoroutine(cameraShake.ScreenShake(0.15f, 0.02f));
            }
            else
            {

            }

            this.lastRocket = Time.time;
            
        }

        CanvasManager.Instance.UpdateWeaponMonitor();
    }

    IEnumerator AmmoHandler()
    {


        if (PlayerStats.instance.ammunitionSupply > 0)
        {
            // reloading coroutine for ammunition
            yield return new WaitForSeconds(2.416667f);
            if(PlayerStats.instance.Ammunition.GetValue() > PlayerStats.instance.ammunitionSupply)
            {
                if(currentProjectile.name == "ProjectileShotgun")
                {
                   clipAmmunition = ((int)PlayerStats.instance.ammunitionSupply - (int)PlayerStats.instance.ammunitionSupply % 5);
                }
                else {
                    clipAmmunition = PlayerStats.instance.ammunitionSupply;
                }
            } else {
                clipAmmunition = PlayerStats.instance.Ammunition.GetValue();
            }
            
            CanvasManager.Instance.UpdateWeaponMonitor();
            isReloading = false;
        }
        else
        {
            isReloading = false;
        }
    }

    private void Reloading()
    {
        StartCoroutine(AmmoHandler());
    }

    private void MeleeAttack()
    {
        SoundManager.PlaySound("Knife");
        // start melee attack
        StartCoroutine(KnifeHandler());
        knifeBack.SetActive(false);
        knifeHand.SetActive(true);
        animator.SetTrigger("slashing");

        StartCoroutine(cameraShake.ScreenShake(0.15f, 0.02f));
    }

    IEnumerator KnifeHandler()
    {
        // knife model visibility handler
        makeMeleeDamage = true;
        yield return new WaitForSeconds(0.4f);
        knifeBack.SetActive(true);
        knifeHand.SetActive(false);
        isMeleeing = false;
    }

    IEnumerator Diving()
    {
        // dodge roll coroutine
        animator.SetTrigger("diving");
        Vector3 dashVelocity = Vector3.Scale(transform.forward,
            4f * new Vector3((Mathf.Log(1f / (Time.deltaTime * characterRigidbody.drag + 1)) / -Time.deltaTime), 0,
                (Mathf.Log(1f / (Time.deltaTime * characterRigidbody.drag + 1)) / -Time.deltaTime)));
        characterRigidbody.AddForce(dashVelocity, ForceMode.VelocityChange);
        yield return new WaitForSeconds(0.396f);
        isDiving = false;
        cooldown = 1.0f;
    }


    private void Interact()
    {
        if (interactableCount > 0)
        {
            if (interactableObjectsNearby[0].gameObject.CompareTag("ElectricalBox"))
            {
                var boxScript = interactableObjectsNearby[0].GetComponent<ElectricityBox>();
                if (boxScript != null)
                    boxScript.DeactivateTower();
            }

            // interaction with a building
            if (nearestInteractBuilding != null &&
                Vector3.Distance(nearestInteractBuilding.gameObject.transform.position, transform.position) < 6)
            {
                if (nearestInteractBuilding.GetComponent<ShopScript>() != null)
                {
                    if (Shop.activeSelf)
                    {
                        Shop.SetActive(false);
                    }
                    else
                    {
                        Shop.SetActive(true);

                        if (useController)
                        {
                            Button btn = GameObject.Find("WeaponButton1").GetComponent<Button>();
                            btn.Select();
                        }
                    }
                }

                if (nearestInteractBuilding.GetComponent<Labor>() != null)
                {
                    if (SkillSystem.activeSelf)
                    {
                        SkillSystem.SetActive(false);
                    }
                    else
                    {
                        SkillSystem.SetActive(true);

                        if (useController)
                        {
                            Button btn = GameObject.Find("SkillButton1").GetComponent<Button>();
                            btn.Select();
                        }
                    }
                }
            }

            lastInteractionEffectTime -= effectRepeatingTime;
        }
    }

    private void UpdateInteractableObjects()
    {
        interactableCount =
            Physics.OverlapSphereNonAlloc(transform.position, interactRadius, interactableObjectsNearby, 1 << 14);

        //Check interactable items
        if (interactableCount > 0)
        {
            if (interactableObjectsNearby[0].gameObject.CompareTag("ElectricalBox"))
            {
                if (!interactableObjectsNearby[0].gameObject.GetComponentInParent<Tower>().isActivated)
                    return;
                //spawn interaction effect and offset in in Y direction
                if (Time.time - lastInteractionEffectTime > effectRepeatingTime)
                {
                    Vector3 effectPos = interactableObjectsNearby[0].gameObject.transform.position;
                    effectPos.y += 1.5f;
                    Instantiate(interactionEffect, effectPos, Quaternion.identity,
                        GameObject.Find("vfx_Container").transform);
                    lastInteractionEffectTime = Time.time;
                }
            }
        }
        //check buildings nearby
        else
        {
            interactableCount = Physics.OverlapSphereNonAlloc(transform.position, interactRadius + 4,
                interactableObjectsNearby, 1 << 9);

            if (interactableCount > 0)
                for (int i = 0; i < interactableCount; i++)
                {
                    if (interactableObjectsNearby[i].GetComponent<ShopScript>() != null ||
                        interactableObjectsNearby[i].GetComponent<Labor>() != null)
                    {
                        nearestInteractBuilding = interactableObjectsNearby[i];
                        break;
                    }
                }
            else
                nearestInteractBuilding = null;
        }

        if (Shop.activeSelf || SkillSystem.activeSelf)
            UpdateShopWindow();
    }

    private void UpdateShopWindow()
    {
        //deactivate the shop and labor if the player is nearby another interactable
        if (nearestInteractBuilding == null)
        {
            SkillSystem.SetActive(false);
            Shop.SetActive(false);
        }
        //deactivate the shop and labor if the player is to far away
        else if (Vector3.Distance(nearestInteractBuilding.gameObject.transform.position, transform.position) > 6)
        {
            SkillSystem.SetActive(false);
            Shop.SetActive(false);
        }
    }

    IEnumerator ShowTooltip()
    {
        var tooltip = GameObject.Find("PlayerInfos");

        if (tooltip)
            tooltip.GetComponent<Text>().text =
                "Find a nearby base and conquer it! \n Build a pump there to refill your water. \n Look around for Pickups and green Lootboxes!";

        yield return new WaitForSeconds(10);

        tooltip.GetComponent<Text>().text = " ";
    }
}