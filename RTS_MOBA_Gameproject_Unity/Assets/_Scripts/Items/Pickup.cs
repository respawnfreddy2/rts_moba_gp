﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public string pickupType;


    void Update()
    {
        transform.Rotate(transform.rotation.x, 50.0f * Time.deltaTime, transform.rotation.z);
    }

    private void OnTriggerEnter (Collider other) {
        if (other.transform.CompareTag("Player")) {
            // add resource
            if (pickupType == "Water") {
                if (PlayerStats.instance.currentWater < 100) {
                    PlayerStats.instance.currentWater = 100;
                    Destroy(gameObject);
                }                 
            }
            else if (pickupType == "Health") {
                if(PlayerStats.instance.currentHealth < PlayerStats.instance.MaxHealth.GetValue()) {
                    
                    if (PlayerStats.instance.currentHealth + 50 > PlayerStats.instance.MaxHealth.GetValue()) {
                        PlayerStats.instance.currentHealth = PlayerStats.instance.MaxHealth.GetValue();
                    }
                    else {
                        PlayerStats.instance.currentHealth += 50;
                    }

                    Destroy(gameObject);
                }                  
            }
            else if (pickupType == "Gears") {
                PlayerStats.instance.Money += 25;
                Destroy(gameObject);
            }
            else if (pickupType == "Ammo") {
                // ammunition bonus depends on active weapon
                PlayerStats.instance.ammunitionSupply += 40;
                CanvasManager.Instance.UpdateWeaponMonitor();
                Destroy(gameObject);
            }         
        }
    }
}