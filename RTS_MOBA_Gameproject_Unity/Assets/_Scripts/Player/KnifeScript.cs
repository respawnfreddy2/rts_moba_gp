﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeScript : MonoBehaviour
{
    public PlayerController player;
    public float meleeDamage;
    public GameObject bulletHit;

    private Transform vfxContainer;


    private void Start () {
         vfxContainer = GameObject.Find("vfx_Container").transform;
    }

    private void OnTriggerStay (Collider other) {
        if (other.transform.CompareTag("Enemy") && player.makeMeleeDamage) {
            player.makeMeleeDamage = false;
            other.transform.GetComponent<EnemyStatsHandler>().DoDamage(meleeDamage);

            Vector3 position = transform.position;
            GameObject vfx = Instantiate(bulletHit, position, transform.rotation, vfxContainer);
            Destroy(vfx, 0.5f);
        }
        else {

        }
    }
}
