﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using UnityEngine.UI;


public class PlayerStats : MonoBehaviour
{
    #region Singleton


    public static PlayerStats instance;

    void Awake()
    {
        instance = this;
    }
    #endregion
    public Weapon testWeapon;
    public int Money;
    public Slider healthslider;
    public Slider waterSlider;
    public Slider epSlider;
    public int skillPoints;
    public bool rockets = false;
    [HideInInspector] public float currentHealth;
    [HideInInspector] public float currentEnergy;
    [HideInInspector] public float currentWater;
    public int ammunitionSupply;
    public Stat Damage;
    public Stat Ammunition;
    public Stat MovementSpeed;
    public float experience;
    public float levelUpEP;
    public float level;
    public Stat MaxHealth;
    public Stat MaxEnergy;
    public Stat MaxWater;
    public Stat WaterConsumptionRate;
    public int Water;
    public Stat FireRate;
    public Stat healrateSeconds;
    public Stat energyRateSeconds;
    public Stat waterRateSecond;
    public GameObject slotQ;


    void Start()
    {
        ItemManager.instance.OnWeaponChanged += OnEquipmentChanged;
        Skillmanager.instance.OnNewSkill +=  OnNewSkillTest;
        GameObject.Find("Canvas/Ammunition").GetComponent<Image>().sprite = ItemManager.instance.primaryWeapon.Icon;
        currentHealth = MaxHealth.GetValue();
        // healthslider.maxValue = MaxHealth.GetValue();
        currentEnergy = MaxEnergy.GetValue();
        experience = 0;
        levelUpEP = 10;
        level = 1;
        skillPoints = 1;
        currentWater = MaxWater.GetValue();
        // waterSlider.maxValue = MaxWater.GetValue();      

        // Gaining Mana and Health only when isAttackingCD is false
        StartCoroutine(AddHealth());
        // StartCoroutine(AddEnergy());
        StartCoroutine(ConsumeWater());
        StartCoroutine(ConsumeLife());
    }

    void Update()
    {
        if (currentHealth > 0)
            DeathScreen.playerIsDead = false;
    }

    void OnEquipmentChanged(Weapon newWeapon)
    {
        if (newWeapon != null)
        {
            Damage.SetBaseValue(newWeapon.damage);
            Ammunition.SetBaseValue(newWeapon.ammunition);
            GameObject.Find("Player").GetComponent<PlayerController>().clipAmmunition = PlayerStats.instance.Ammunition.GetValue();
            GameObject.Find("Player").GetComponent<PlayerController>().currentProjectile = newWeapon.projectile;
            GameObject.Find("Player/Character_Prototype/Player/Hip/Waist/Chest/R.Shoulder/R.UpArm/R.LoArm/R.Hand/Gun").GetComponent<MeshFilter>().sharedMesh = Instantiate(newWeapon.weaponModel).GetComponent<MeshFilter>().mesh;
            GameObject.Find("Player").GetComponent<PlayerController>().TriggerReloadAnimation();
            GameObject.Find("Canvas/Ammunition").GetComponent<Image>().sprite = newWeapon.Icon;
            // setWeaponUIAlpha(); - wäre super

        }      
    }

    void OnNewSkillTest (Skill newSkill)
    {
        if(newSkill.damageModifier!= 0)
        {
            PlayerStats.instance.Damage.AddModifier(newSkill.damageModifier);
        }
         if(newSkill.fireRateModifier!= 0)
        {
            PlayerStats.instance.FireRate.AddModifier(newSkill.fireRateModifier);
        
        }
        if(newSkill.ammuntionModifier!= 0)
        {
            PlayerStats.instance.Ammunition.AddModifier(newSkill.ammuntionModifier);
        }

        if(newSkill.healthModifier!= 0)
        {
            PlayerStats.instance.MaxHealth.AddModifier(newSkill.healthModifier);
            PlayerStats.instance.currentHealth = PlayerStats.instance.MaxHealth.GetValue();
        }
        if (newSkill.isAbility)
        {
            rockets = true;
            slotQ.SetActive(true);

        }
        // if(newSkill.utilityModifier!= 0)
        // {
        //     PlayerStats.instance.Damage.AddModifier(newSkill.utilityModifier);
        // }       
    }

    public void GetDamage(float damageAmount)
    {
        SoundManager.PlaySound("GetDamage");

            CanvasManager.Instance.flash();
        //The Return is here to get a Reward for Damage other Enemies or make a Kill
        if (currentHealth > 0)
        {
            currentHealth -= (int)damageAmount;
            // healthslider.value = currentHealth;
            //print("Player bekommt: " + damageAmount + " Schaden!\nPlayer currentHealth: " + currentHealth);
            if (currentHealth <= 0)
            {
                // print("Player dead");
                DeathScreen.playerIsDead = true;
            }
        }
    }

  
    IEnumerator ConsumeWater()
    {
        //loops forever
        while (true)
        {
            if (currentWater > 0)
            {
                currentWater--;
                // waterSlider.value = currentWater;
                yield return new WaitForSeconds(WaterConsumptionRate.GetValue());
            }
            else yield return null;
        }
    }

    IEnumerator ConsumeLife () {
        //loops forever
        while (true) {
            if (currentWater <= 0) {
                currentHealth--;
            
                // healthslider.value = (int)currentHealth;
                yield return new WaitForSeconds(WaterConsumptionRate.GetValue());
            }
            else yield return null;
        }
    }

    IEnumerator AddHealth()
    {
        while (true)
        {
            // loops forever...
            if (currentHealth < MaxHealth.GetValue() && currentWater >= 90)
            {
                currentHealth += 1; 
                yield return new WaitForSeconds(healrateSeconds.GetValue());
            }
            else
            {
                yield return null;
            }
        }
    }
 
    public static float Round(float value, int digits)
    {
        float mult = Mathf.Pow(10.0f, (float)digits);
        return Mathf.Round(value * mult) / mult;
    }

    public void GainEP(float ep)
    {
        this.experience += ep;
        this.experience = Round(this.experience, 1);

        if (this.experience >= levelUpEP)
        {
            LevelUP();
        }
    }

    public void LevelUP()
    {
        this.experience = this.experience - levelUpEP;
        this.skillPoints++;
      
        this.level += 1;
        this.levelUpEP = Round(levelUpEP * 1.4f, 1);
   
    }
    // void setWeaponUIAlpha()
    // {
    //     slot2.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = newItem.Icon;

    //     GameObject.Find("Canvas/Ammunition").GetComponent<Image>().sprite = newWeapon.Icon;

    // }
    //TODO Alpha setzen wäre super
}