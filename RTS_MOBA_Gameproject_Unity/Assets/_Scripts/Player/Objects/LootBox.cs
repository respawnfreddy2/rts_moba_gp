﻿using UnityEngine;

namespace _Scripts.Items
{
    public class LootBox : MonoBehaviour
    {
        public Animator animator;
        public GameObject [] lootList;
        public GameObject tooltip;
        private Transform Player;
        private bool hasBeenOpenend = false;      


        private void Awake () {
            Player = GameObject.FindWithTag("Player").transform;
            tooltip.SetActive(false);
        }

        private void Update () {
            if (Input.GetButtonDown("Interact") && Vector3.Distance(Player.position, transform.position) < 3.0f) {
                openLootbox();
            }

            if (Vector3.Distance(Player.position, transform.position) < 3.0f) {
                tooltip.SetActive(true);
            }
            else {
                tooltip.SetActive(false);
            }
                
        }
        public void openLootbox() {
            if (!hasBeenOpenend && lootList.Length > 0) {
                SoundManager.PlaySound("OpenChest");
                
                int index = Random.Range(0, lootList.Length);
                GameObject loot = lootList [index];

                Instantiate(loot, transform.position, transform.rotation, GameObject.Find("Items").transform);
                hasBeenOpenend = true;

                Destroy(transform.parent.gameObject, 2);
            }
            if (animator.GetBool("open")) {
                animator.SetBool("open", false);        
            }
            else {
                animator.SetBool("open", true);
            }
        }
    }
}
