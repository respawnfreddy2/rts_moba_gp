﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    public float barrelHealth;
    public GameObject explosionVFX;
    public CameraShake cameraShake;


    private void Awake () {
        cameraShake = GameObject.Find("MainCamera").GetComponent<CameraShake>();
        barrelHealth = 75;
    }

    public void DoDamage(float damage) {
        
        if(barrelHealth< 0)
            return;
        
        barrelHealth -= (int)damage;

        if (barrelHealth <= 0) {
            GameObject vfxContainer = GameObject.Find("vfx_Container");
            var hitVfx = Instantiate(explosionVFX, transform.position, transform.rotation, vfxContainer.transform);

            StartCoroutine(cameraShake.ScreenShake(0.5f, 0.3f));
            Destroy(hitVfx, 3);
            Destroy(gameObject,1);
        }       
    }
}
