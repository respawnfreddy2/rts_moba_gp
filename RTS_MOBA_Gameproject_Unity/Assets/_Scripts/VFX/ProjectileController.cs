﻿using UnityEngine;

namespace _Scripts.VFX
{
    public class ProjectileController : MonoBehaviour
    {
    
        public GameObject bulletHit;
        public float projectileSpeed;
        public float damage;
        public float fireRate;
    
        public GameObject flashEffect;
        protected GameObject flashEffectInstantiate;
    }
}
