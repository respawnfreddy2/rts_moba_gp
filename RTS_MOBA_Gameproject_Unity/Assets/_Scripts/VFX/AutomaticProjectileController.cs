﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace _Scripts.VFX
{
    public class AutomaticProjectileController : ProjectileController
    {
        private GameObject vfxContainer;
        [HideInInspector] public bool ignoreEnemy = true;


        private void Start() {
            
            
            if (GameObject.Find("vfx_Container")) {
                vfxContainer = GameObject.Find("vfx_Container");
            }

              
            flashEffectInstantiate = Instantiate(flashEffect, transform.position, transform.rotation, transform);
            Destroy(flashEffectInstantiate, 0.2f);
            Destroy(gameObject, 2.5f);
        }

        void Update()
        {
            if (projectileSpeed != 0) {
                transform.position += transform.forward * (projectileSpeed * Time.deltaTime);
            
                if(flashEffectInstantiate != null)
                    flashEffectInstantiate.transform.position -= transform.forward * (projectileSpeed * Time.deltaTime);
            }
        }

        void OnCollisionEnter (Collision collision) {       
            if (collision.transform.CompareTag("Enemy") && ignoreEnemy)
            {           
                collision.transform.GetComponent<EnemyStatsHandler>().DoDamage(damage);
            }

            if (collision.transform.CompareTag("Barrel")) {
                collision.transform.GetComponent<Barrel>().DoDamage(damage);
            }

            if (collision.transform.CompareTag("Player")) {
                collision.transform.GetComponent<PlayerStats>().GetDamage(damage);
            }

            if (collision.transform.CompareTag("ElectricalBox"))
            {
                collision.transform.GetComponent<ElectricityBox>().doDamage(damage);
            }

            ContactPoint contact = collision.contacts[0];
            Vector3 position = contact.point;
            if (bulletHit != null && vfxContainer != null)
            {
                GameObject vfx = Instantiate(bulletHit, position, transform.rotation, vfxContainer.transform);
                Destroy(vfx, 0.5f);
            }
            Destroy(gameObject);
        }
    }
}
