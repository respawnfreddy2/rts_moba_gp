﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RocketSlot : MonoBehaviour

{
    public float coolDown = 20;
    public float coolDownTimer;
    public Text text;

    private PlayerController playerController;
    void Start()
    {
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    void Update()
    {
        if(playerController.lastRocket + 20 > Time.time)
        {
            text.text = "" + (int)(playerController.lastRocket + 20 - Time.time);
        }
        else {
             text.text = "READY";
        }
  

    
    }
    

}
